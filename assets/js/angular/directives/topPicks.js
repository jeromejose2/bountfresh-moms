'use strict';
angular.module('topPicks',[])
.directive('topPicks',['$window',function($window){

	return {
		restrict : "A",
		link: function(scope,element,attrs){


            $($window).on('scroll',function(){
	            var scrollTop = $(this).scrollTop();
	            var el = $('.top-picks');
	            var topLeft = el.offset().left ;
	             if(!el.hasClass('fixed-location')){
	                 if(scrollTop >= 696){
	                    console.log(topLeft);
	                    el.addClass('fixed-location');
	                    el.css({'left':topLeft});

	                 }else{
	                    el.removeClass('fixed-location');
	                 }
	             }else{
	                  if(scrollTop < 696){
	                      el.removeClass('fixed-location');
	                      el.css({'left':'initial'})
	                  }
	             }

	    });

		}
	}
}]);
