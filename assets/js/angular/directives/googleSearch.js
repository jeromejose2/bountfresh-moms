'use strict';

angular.module('googleSearch',[])
.directive('googlesearch',['$window',function($window){

	return {
		restrict : "A",
		link: function(scope,element,attrs){

			element.on('keydown', function (event) {				
				 
                 if (event.which==13) {
                 	$window.location.href="search/"+element.val().replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
                }                 

            });

		}
	}
}]);