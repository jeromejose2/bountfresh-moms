'use strict';

angular.module('imageSlider',[])

.directive('owlcarousel',function(){
	var linker = function(scope,element,attrs){

		var loadCarousel = function(){

			element.owlCarousel({
				 navigation : true, // Show next and prev buttons
		          slideSpeed : 300,
		          paginationSpeed : 400,
		          singleItem:true
			});
		}		 
 		  		 
		scope.$watch("header_images", function(value) {

			if(scope.header_images.length > 0){
				var html = [];
				var c = false;
				var template = '<div class="home-msg" class="{{ scope.d }}"><div ng-show="!login_status" class="not-logged-in"><div class="col-md-6 col-md-offset-2"><p>Be part of the Bounty Fresh Moms! Registration is for FREE! You get access to latest news and updates, promotions and the best recipes we have to offer!</p></div><div class="col-md-4 text-center"><a href="register" class="btn">JOIN NOW!</a></div></div></div></div>';
 				
				angular.forEach(scope.header_images, function(value, key){
				if(value.link) {
   		          		html.push('<div class="item cht" ><a href="'+value.link+'" target="_blank"><img src="'+value.image+'" class="img-responsive" alt="Slider"/>' + template + '</a></div>');
				} else {
					html.push('<div class="item" ><img src="'+value.image+'" class="img-responsive" alt="Slider"/></div>');
				}
				console.log(value.link);
   		          	c = true;
		        });		       

		        if(c){
 		        	element.append(html.join(''));
		        	loadCarousel(element);
		        }		        
			}	

		});

	}

	return {
		restrict : "A",
		link: linker
	}

})

.directive('owltext',function(){
	var linker = function(scope,element,attrs){

		var loadCarousel = function(){

			element.owlCarousel({
				 navigation : true, // Show next and prev buttons
		          slideSpeed : 300,
		          paginationSpeed : 400,
		          singleItem:true
			});
		}	}	 

	return {
		restrict : "A",
		link: linker
	}

})