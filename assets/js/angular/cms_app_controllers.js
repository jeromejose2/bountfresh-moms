var CMSAppControllers = angular.module('CMSAppControllers',['serviceControllers','angularFileUpload','ngDialog','textAngular']);

CMSAppControllers.controller('mainCtrl',['$rootScope','$scope','$route','$http','$resource','$window','ngDialog',function($rootScope,$scope,$route,$http,$resource,$window,ngDialog){

	$scope.statusDialog = null;
	$scope.notifyDialog = null;

	$rootScope.$on('$routeChangeStart',function(){

		$scope.openStatusDialog();

	});

 	$rootScope.$on('$routeChangeSuccess',function(){
		$rootScope.pageTitle = $route.current.title;
		if($rootScope.pageTitle != "Registrants") {
			$scope.statusDialog.close();
		}

		$http.get('../api/cms/login_check').success(function(data){
			if(data.login_status=='failed'){
				$rootScope.login_status = false;
  				$window.location.href = '#/login';
			}else{
				$rootScope.login_status = true;
			}
		});

	});

	$scope.logout = function()
	{
 
		$http.get('../api/cms/logout').success(function(data){
			$rootScope.login_status = false;
  			$window.location.href = '#/login';			 
		});

	}

	$scope.openStatusDialog = function () {
        
        $scope.statusDialog = ngDialog.open({
								template: 'openStatusDialog',
								closeByDocument: false,
								closeByEscape: false,
								showClose:false
							});

    };

     $scope.openNotify = function (message) {
		
		$scope.notifyDialog = ngDialog.open({
								template: '<p>'+message+'</p>',
					 			plain: true
							}); 
	};


	
}]);


CMSAppControllers.controller('loginCtrl',['$rootScope','$scope','$http','$window',function($rootScope,$scope,$http,$window){

 	$scope.login = function()
	{

		var param = $scope.user;
		$scope.login_message = false;

		$http.post('../api/cms/login',param).success(function(data){

			if(data.logged_in==true){
				$rootScope.login_status=true;
				$window.location.href = '#/home';
			}else{
				$rootScope.login_status = false;
				$scope.login_message = data.login_message;
			}
		});

	}

	
 	 
}]); 


CMSAppControllers.controller('homeCtrl',['$scope','$http','Graph','Summary_Registrants','Summary_Comments','Summary_Registrants_Month','Summary_Comments_Month',function($scope,$http,Graph,Summary_Registrants,Summary_Comments,Summary_Registrants_Month,Summary_Comments_Month){

 	 $scope.chart = {};
 	 $scope.summaryRegistrants = [];
 	 $scope.summaryComments = [];
 	 $scope.summaryRegistrantsMonth = [];
 	 $scope.summaryCommentsMonth = [];

 	 $scope.loadItems = function() {

 	 	$scope.chart = Graph.get();
 	 	$scope.summaryRegistrants = Summary_Registrants.get();
 	 	$scope.summaryComments = Summary_Comments.get();
 	 	$scope.summaryRegistrantsMonth = Summary_Registrants_Month.get();
 	 	$scope.summaryCommentsMonth = Summary_Comments_Month.get();

 	 }

 	 $scope.loadItems();

}]);


CMSAppControllers.controller('recipesListCtrl',['$scope','$window','$resource','Recipe','$routeParams','$route',function($scope,$window,$resource,Recipe,$routeParams,$route){
 	
 	$scope.items = [];
 	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-recipes' : $routeParams.page;
 	$scope.page = $routeParams.page;

	$scope.deleteItem = function(item){
 		if(confirm('Are you sure you want to delete?')){ 
 			  item.$delete({id:item.id},function(data){
			  		 $scope.loadItems();			  	
			  }); 
		}		
	};

	$scope.loadItems = function(){
		$scope.items = Recipe.query({page:$routeParams.page});
	};

	$scope.loadItems();
	  
}]);

CMSAppControllers.controller('recipesAddCtrl',['$scope','$route','$http','Recipe','$window','$upload','$timeout','$routeParams',function($scope,$route,$http,Recipe,$window,$upload,$timeout,$routeParams){

	$scope.recipe = new Recipe();
	$scope.selectedFiles = [];
	$scope.assetBaseURL = '../assets/';
	$scope.apiBaseURL = '../api/';
	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.errorFields = false;
 	$scope.progress = -1;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-recipes' : $routeParams.page;
	$scope.recipe.page = $routeParams.page;

  
	$scope.addItem = function(){

		$scope.errorFields = false;

		if($scope.recipe.image || $scope.recipe.title || $scope.recipe.description){

			$scope.recipe.$save(function(data){
 				$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
			});

		}else{
			$scope.errorFields = 'Please fill out all fields.';
			return;
		}
		
	}; 

	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);

		}else{
			$scope.errorUpload = 'The filetype you are attempting to upload is not allowed.';
			return;
		}



		$upload.upload({
				url : $scope.apiBaseURL+'upload/recipe',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;
				if(!response.error){
					$scope.recipe.image = response.new_image;
 				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('')}, false);
			});

	};

 
 		
}]);


CMSAppControllers.controller('recipesEditCtrl',['$route','$scope','Recipe','$routeParams','$window','$upload','$http','$timeout',function($route,$scope,Recipe,$routeParams,$window,$upload,$http,$timeout){

	$scope.recipe = new Recipe();
  	$scope.selectedFiles = false;
  	$scope.assetBaseURL = '../assets/';
  	$scope.apiBaseURL = '../api/';
  	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.errorFields = false;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-recipes' : $routeParams.page;
	$scope.recipe.page = $routeParams.page;
  
  
	$scope.updateItem = function(){
		
		$scope.errorFields = false;

		if($scope.recipe.image || $scope.recipe.title || $scope.recipe.description){ 
			$scope.recipe.$update(function(data){
 				$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
			});
		}else{
			$scope.errorFields = 'Please fill out all fields.';
			return;
		}

  	};

	$scope.loadItem = function(){
		$scope.recipe = Recipe.get({id:$routeParams.id,page:$scope.page});
   	};

  


  	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);
		}else{
			$scope.errorUpload = 'The filetype you are attempting to upload is not allowed.';
			return;
		}

		$upload.upload({
				url : $scope.apiBaseURL+'upload/recipe',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;
				if(!response.error){
					$scope.recipe.image = response.new_image;
				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {
				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
				
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('')}, false);
			});

	};

 
	$scope.loadItem();



}]);


CMSAppControllers.controller('articlesListCtrl',['$scope','$window','$resource','Article','$route','$routeParams',function($scope,$window,$resource,Article,$route,$routeParams){
 	
	$scope.items = [];
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-articles' : $routeParams.page;
	$scope.page = $routeParams.page;

	$scope.deleteItem = function(item){
 		if(confirm('Are you sure you want to delete?')){
			
		    item.$delete({id:item.id},function(){
				$scope.loadItems();
			});

		}		
	};

	$scope.loadItems = function()
	{
		$scope.items = Article.query({page:$routeParams.page});
	}

	$scope.loadItems();
	  
}]);

CMSAppControllers.controller('articlesAddCtrl',['$scope','$http','Article','$window','$upload','$timeout','$routeParams',function($scope,$http,Article,$window,$upload,$timeout,$routeParams){

	$scope.article = new Article();
	$scope.selectedFiles = [];
	$scope.assetBaseURL = '../assets/';
	$scope.apiBaseURL = '../api/';
	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.errorFields = false;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-articles' : $routeParams.page;
	$scope.article.page = $routeParams.page;
 

  
	$scope.addItem = function(){

		$scope.errorFields = false;

		if($scope.article.image || $scope.article.title || $scope.article.description){ 

			$scope.article.$save(function(data){
				$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
			});

		}else{
			$scope.errorFields = 'Please fill out all fields.';
			return;
		}
		
	};
 
 
	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);
		}


		$upload.upload({
				url : $scope.apiBaseURL+'upload/article',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;

 				if(!response.error){
					$scope.article.image = response.new_image;
				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('')}, false);
			});

	};
 

 
 		
}]);


CMSAppControllers.controller('articlesEditCtrl',['$scope','Article','$routeParams','$window','$upload','$http','$timeout',function($scope,Article,$routeParams,$window,$upload,$http,$timeout){

	$scope.article = new Article();
  	$scope.selectedFiles = false;
  	$scope.assetBaseURL = '../assets/';
  	$scope.apiBaseURL = '../api/';
  	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.errorFields = false;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-articles' : $routeParams.page;
	$scope.article.page = $routeParams.page;



	$scope.updateItem = function(){
		
		$scope.errorFields = false;

		if($scope.article.image || $scope.article.title || $scope.article.description){  
			$scope.article.$update(function(data){
				$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
			});
		}else{
			$scope.errorFields = 'Please fill out all fields.';
			return;
		}
  
	};

	$scope.loadItem = function(){false;false;false;false;false;false;false;false;false;false;false;false;
		$scope.article = Article.get({id:$routeParams.id,page:$routeParams.page});		
  	};
 
   
  	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);
		}


		$upload.upload({
				url : $scope.apiBaseURL+'upload/article',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;

				if(!response.error){
					$scope.article.image = response.new_image;
				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {
				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
				
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('')}, false);
			});

	};

 	$scope.loadItem();
 
}]);

CMSAppControllers.controller('image_headersListCtrl',['$scope','$window','$resource','Image_header','$route',function($scope,$window,$resource,Image_header,$route){
 	
	$scope.items = [];
 

	$scope.deleteItem = function(item){
 		if(confirm('Are you sure you want to delete?')){
 
		    item.$delete({id:item.id},function(){
				$scope.loadItems();
			});
		}		
	};

	$scope.loadItems = function()
	{
		$scope.items = Image_header.query();
	};

	$scope.loadItems();

 	  
}]);


CMSAppControllers.controller('image_headersAddCtrl',['$scope','$window','$resource','Image_header','$route','$http','$timeout','$upload',function($scope,$window,$resource,Image_header,$route,$http,$timeout,$upload){
 	
 
	$scope.image_header = new Image_header();

 	$scope.assetBaseURL = '../assets/';
  	$scope.apiBaseURL = '../api/';
  	$scope.image_headers = [];

  	$scope.errorFields = false;
 

	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};

	$scope.onFileSelect = function($files)
	{
		$scope.selectedFiles = $files;
		$scope.upload = [];		
		$scope.dataUrls = [];
		$scope.progress = [];

		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}

		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
 			$scope.start(i);
		}


	};

	$scope.start = function(index) {

		$scope.progress[index] = 0;	
		$scope.errorUpload = false;

		$scope.upload[index] = $upload.upload({
			url : $scope.apiBaseURL+'upload/image_header',
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'image'
		}).then(function(response) {
			var response = response.data;
			
			if(!response.error){
				$scope.image_headers[index] = response.new_image;
			}else{
				$scope.errorUpload = response.error;
			}
 
		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('')}, false);
		});

	}


	$scope.remove = function(index) {

		var param = {image:$scope.image_headers[index]};

  		$http({url: $scope.apiBaseURL+'upload/remove_header',params:param,method:'get'})
		.success(function(response){
			console.log('');
		});
		 
		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.errorUpload.splice(index, 1);
 
	}


	$scope.addItem = function()
	{
		$scope.errorFields = false; 

		if($scope.image_header.link){  
			$scope.image_header.images = $scope.image_headers;
			$scope.image_header.$save(function(data){
				$window.location.href = '#/image_headers';
			});
		}else{
			$scope.errorFields = 'Please fill out all fields.';
			return;
		}

	}
 
	  
}]);

CMSAppControllers.controller('registrantsListCtrl',['$scope','Registrant',function($scope,Registrant){

	var clickedPages = [];
	var domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port : '');
	var siteUrl = domain+"/api/cms/registrants";
	var siteUrlExport = domain+"/api/cms/export_registrants";
	var siteUrlDateAddedFilter = domain+"/api/cms/filter_date_added";

	$scope.itemsPerPage = 15;
	  $scope.currentPage = 0;
	  $scope.items = [];
	  $scope.pCount = 1000;
	  $scope.predicate = 'timestamp';
	  $scope.reverse = true;

	  $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
      };

	  $scope.loadItems = function() {
	  	$scope.items = Registrant.query();
	  	setTimeout(function() {
	  		$.get(siteUrl, {'offset' : 1000, 'bulk_load' : 1}, function(response){
		   		for(l = 0; l <= response.length; l++) {
		   			$scope.items.push(response[l]);
		   		}
		   		
		   		$scope.pCount = $scope.items.length;
	  			$scope.statusDialog.close();
		   		setTimeout(function() {
		   			$.get(siteUrl, {'offset' : 6000, 'bulk_load' : 1}, function(response){
		   				for(l = 0; l <= response.length; l++) {
				   			if($scope.items.indexOf(response[l]) == -1) {
				   				$scope.items.push(response[l]);
				   			}
				   		}
				   		$scope.pCount = $scope.items.length;
				   		
				   		setTimeout(function() {
				   			$.get(siteUrl, {'offset' : 11000, 'bulk_load' : 1}, function(response){
				   				for(l = 0; l <= response.length; l++) {
						   			if($scope.items.indexOf(response[l]) == -1) {
						   				$scope.items.push(response[l]);
						   			}
						   		}
						   		$scope.pCount = $scope.items.length;
						   		
						   		setTimeout(function() {
						   			$.get(siteUrl, {'offset' : 16000, 'bulk_load' : 1}, function(response){
						   				for(l = 0; l <= response.length; l++) {
								   			if($scope.items.indexOf(response[l]) == -1) {
								   				$scope.items.push(response[l]);
								   			}
								   		}
								   		$scope.pCount = $scope.items.length;
								   		
								   		setTimeout(function() {
							   			$.get(siteUrl, {'offset' : 21000, 'bulk_load' : 1}, function(response){
							   				for(l = 0; l <= response.length; l++) {
									   			if($scope.items.indexOf(response[l]) == -1) {
									   				$scope.items.push(response[l]);
									   			}
									   		}
									   		$scope.pCount = $scope.items.length;
									   		
							   			});
									});
						   			});
								});
				   			});
						});
		   			});
				});
			});	  		
	  	}, 1000);
	  }

	  $scope.range = function(input) {
	  	$scope.pCount = input;
	  	if(input && input !== 15) {
	  		var divider = 15;
	  		var floor = Math.floor(input / divider);
	  		var input = floor + 1;
	  		if(input > 6 ) {
	  			input = 5;
	  		} else if(input === 0) {
	  			input = 0;
	  		}
	  	} else if(input === 0) {
	  		input = 0;
	  	} else {
	  		input = 5;
	  	}
	    var rangeSize = input;
	    var ret = [];
	    var start;

	    start = $scope.currentPage;
	    if ( start > $scope.pageCount()-rangeSize ) {
	    	start = 0;
	      // start = $scope.pageCount()-rangeSize+1;
	    }

	    for (var i=start; i<start+rangeSize; i++) {
	      ret.push(i);
	    }
	    return ret;
	  };

	  $scope.prevPage = function() {
	    if ($scope.currentPage > 0) {
	      $scope.currentPage--;
	    }
	  };

	  $scope.prevPageDisabled = function() {
	    return $scope.currentPage === 0 ? "disabled" : "";
	  };

	  $scope.pageCount = function() {
	    return Math.ceil($scope.pCount/$scope.itemsPerPage)-1;
	  };

	  $scope.nextPage = function() {
	    if ($scope.currentPage < $scope.pageCount()) {
	      $scope.currentPage++;
	    }
	  };

	  $scope.nextPageDisabled = function() {
	    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	  };

	  $scope.resetPagination = function() {
	  	if($scope.currentPage != 0) {
	  		$scope.currentPage = 0;
	  	}
	  };

	  $scope.fooo = function() {
		var date_added = $('input').eq(4).val();
		$.get(siteUrlDateAddedFilter, {'date_added':date_added}, function(response) {
			if(response == "") {
				console.log('nothing');
			} else {
				console.log($scope.items);
				
				for(m = 0; m <= response.length; m++) {

					if($scope.items.indexOf(response[m].id) == -1) {
						// console.log('wala');
						$scope.items.push(response[m]);
						
					} else {
						// $scope.items.push(response[m]);
						// console.log('meron');
					}
				}
			}
		});
	}

	  $scope.setPage = function(n) {
	    $scope.currentPage = n;
	  };

	  $scope.deleteItem = function(item){

	 		if(confirm('Are you sure you want to delete?')){
				
			    item.$delete({id:item.id},function(){
					$scope.loadItems();
				});

			}		
		};
	$scope.exportData = function () {
		var name = $('input').eq(0).val();
		var mobile = $('input').eq(1).val();
		var birthdate = $('input').eq(2).val();
		var gender = $('select').eq(0).val();
		var email = $('input').eq(3).val();
		var status = $('select').eq(1).val();
		var date_added = $('input').eq(4).val();

		window.location.href=siteUrlExport+"?name="+name+"&mobile="+mobile+"&birthdate="+birthdate+"&gender="+gender+"&email="+email+"&status="+status+"&date_added="+date_added;
	};

		$scope.loadItems();

}]);

CMSAppControllers.controller('privacyPolicyListCtrl',['$scope','Privacy_policy',function($scope,Privacy_policy){

	$scope.privacy_policy = new Privacy_policy();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.privacy_policy = Privacy_policy.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.privacy_policy.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('termsAndConditionsListCtrl',['$scope','Terms_and_conditions',function($scope,Terms_and_conditions){

	$scope.terms_and_conditions = new Terms_and_conditions();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.terms_and_conditions = Terms_and_conditions.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.terms_and_conditions.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('bountyFreshFoodsListCtrl',['$scope','Bounty_fresh_foods',function($scope,Bounty_fresh_foods){

	$scope.bounty_fresh_foods = new Bounty_fresh_foods();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.bounty_fresh_foods = Bounty_fresh_foods.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.bounty_fresh_foods.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('bountyAgroVenturesListCtrl',['$scope','Bounty_agro_ventures',function($scope,Bounty_agro_ventures){

	$scope.bounty_agro_ventures = new Bounty_agro_ventures();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.bounty_agro_ventures = Bounty_agro_ventures.get({id:2});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.bounty_agro_ventures.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('faqEditCtrl',['$scope','FAQ',function($scope,FAQ){

 
	$scope.faq = new FAQ();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.faq = FAQ.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.faq.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);


CMSAppControllers.controller('recipeCommentListCtrl',['$scope','Recipe_Comment',function($scope,Recipe_Comment){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Recipe_Comment.query();
 	}

	 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		if(confirm('Are you sure you want to approve?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		if(confirm('Are you sure you want to disapprove?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		if(confirm('Are you sure you want to make it pending?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('recipeCommentRepliesListCtrl',['$scope','Recipe_Comment_Replies',function($scope,Recipe_Comment_Replies){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Recipe_Comment_Replies.query();
 	}

	 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		if(confirm('Are you sure you want to approve?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		if(confirm('Are you sure you want to disapprove?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		if(confirm('Are you sure you want to make it pending?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('articleCommentListCtrl',['$scope','Article_Comment',function($scope,Article_Comment){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Article_Comment.query();
 	}
 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		item.$update(function(data){
			 $scope.openNotify(data.message);
			 $scope.loadItems();
 		});

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('articleCommentRepliesListCtrl',['$scope','Article_Comment_Replies',function($scope,Article_Comment_Replies){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Article_Comment_Replies.query();
 	}
 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		item.$update(function(data){
			 $scope.openNotify(data.message);
			 $scope.loadItems();
 		});

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('accountsCtrl',['$scope','Accounts', '$http',function($scope, Accounts, $http){

	$scope.items = {};

	$scope.loadItems = function() {
		$scope.items = Accounts.query();
	}

	$scope.updateAccount = function(id) {
		var form = $('#form-'+id).serialize();
		$.post('../api/cms/accounts', form, function(response) {
			window.location.reload();
		});
	}

	$scope.deleteAccount = function(id) {
		if(confirm('delete this?')) {
			$.post('../api/cms/delete_account', {id:id}, function() {
				$scope.loadItems();
			});
		}
	}

	$scope.newAccount = function() {
		var form = $('#form-new').serialize();
		$.post('../api/cms/accounts_new', form, function() {
			$scope.loadItems();
			$('#popup-new').modal('hide');
		});
	}

	$scope.loadItems();

}]);

CMSAppControllers.controller('pledgesListCtrl',['$scope','Pledge',function($scope,Pledge){

	var clickedPages = [];
	var domain = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port : '');
	var siteUrl = domain+"/api/cms/pledges";
	var siteUrlExport = domain+"/api/cms/export_pledges";
	var siteUrlDateAddedFilter = domain+"/api/cms/filter_date_added";
	var siteUrlBdateFilter = domain+"/api/cms/filter_bdate_range";

	$scope.itemsPerPage = 15;
	  $scope.currentPage = 0;
	  $scope.items = [];
	  $scope.pCount = 1000;
	  $scope.predicate = 'date_pledged';
	  $scope.reverse = true;

	  $scope.order = function(predicate) {
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
        $scope.predicate = predicate;
      };

	  $scope.loadItems = function() {
	  	$scope.items = Pledge.query();

	  	setTimeout(function() {
	  		$.get(siteUrl, {'offset' : 1000, 'bulk_load' : 1}, function(response){
		   		for(l = 1; l <= response.length; l++) {
		   			$scope.items.push(response[l]);
		   		}
		   		
		   		$scope.pCount = $scope.items.length;
	  			$scope.statusDialog.close();
		   		setTimeout(function() {
		   			$.get(siteUrl, {'offset' : 6000, 'bulk_load' : 1}, function(response){
		   				for(l = 1; l <= response.length; l++) {
				   			if($scope.items.indexOf(response[l]) == -1) {
				   				$scope.items.push(response[l]);
				   			}
				   		}
				   		$scope.pCount = $scope.items.length;
				   		
				   		setTimeout(function() {
				   			$.get(siteUrl, {'offset' : 11000, 'bulk_load' : 1}, function(response){
				   				for(l = 1; l <= response.length; l++) {
						   			if($scope.items.indexOf(response[l]) == -1) {
						   				$scope.items.push(response[l]);
						   			}
						   		}
						   		$scope.pCount = $scope.items.length;
						   		
						   		setTimeout(function() {
						   			$.get(siteUrl, {'offset' : 16000, 'bulk_load' : 1}, function(response){
						   				for(l = 1; l <= response.length; l++) {
								   			if($scope.items.indexOf(response[l]) == -1) {
								   				$scope.items.push(response[l]);
								   			}
								   		}
								   		$scope.pCount = $scope.items.length;
								   		
								   		setTimeout(function() {
							   			$.get(siteUrl, {'offset' : 21000, 'bulk_load' : 1}, function(response){
							   				for(l = 1; l <= response.length; l++) {
									   			if($scope.items.indexOf(response[l]) == -1) {
									   				$scope.items.push(response[l]);
									   			}
									   		}
									   		$scope.pCount = $scope.items.length;
									   		
							   			});
									});
						   			});
								});
				   			});
						});
		   			});
				});
			});	  		
	  	}, 1000);
	  }

	  $scope.range = function(input) {
	  	$scope.pCount = input;
	  	if(input && input !== 15) {
	  		var divider = 15;
	  		var floor = Math.floor(input / divider);
	  		var input = floor + 1;
	  		if(input > 6 ) {
	  			input = 5;
	  		} else if(input === 0) {
	  			input = 0;
	  		}
	  	} else if(input === 0) {
	  		input = 0;
	  	} else {
	  		input = 1;
	  	}
	    var rangeSize = input;
	    var ret = [];
	    var start;

	    start = $scope.currentPage;
	    if ( start > $scope.pageCount()-rangeSize ) {
	    	start = 0;
	      // start = $scope.pageCount()-rangeSize+1;
	    }

	    for (var i=start; i<start+rangeSize; i++) {
	      ret.push(i);
	    }
	    return ret;
	  };

	  $scope.prevPage = function() {
	    if ($scope.currentPage > 0) {
	      $scope.currentPage--;
	    }
	  };

	  $scope.prevPageDisabled = function() {
	    return $scope.currentPage === 0 ? "disabled" : "";
	  };

	  $scope.pageCount = function() {
	    return Math.ceil($scope.pCount/$scope.itemsPerPage)-1;
	  };

	  $scope.nextPage = function() {
	    if ($scope.currentPage < $scope.pageCount()) {
	      $scope.currentPage++;
	    }
	  };

	  $scope.nextPageDisabled = function() {
	    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
	  };

	  $scope.resetPagination = function() {
	  	if($scope.currentPage != 0) {
	  		$scope.currentPage = 0;
	  	}
	  };

	$scope.bdate_range_filter = function() {
		var date_from = $('#date_from').val();
		var date_to = $('#date_to').val();

		$.get(siteUrlBdateFilter, {'bdate_from':date_from, 'bdate_to':date_to}, function(response) {
			if(response == "") {
				console.log('nothing');
			} else {

				for(m = 0; m <= response.length - 1; m++) {
					console.log('epic');

					if($scope.items.indexOf(response[m].id) == -1) {
						$scope.items.push(response[m]);

						console.log($scope.items);
					} else {
						$scope.items.push(response[m]);
						console.log('meron');
					}
				}
			}

			$scope.loadItems();

		});


	}

	  $scope.setPage = function(n) {
	    $scope.currentPage = n;
	  };

	  $scope.deleteItem = function(item){

	 		if(confirm('Are you sure you want to delete?')){
				
			    item.$delete({id:item.id},function(){
					$scope.loadItems();
				});

			}		
		};
	$scope.exportData = function () {
		var name = $('input').eq(0).val();
		var birthdate = $('input').eq(1).val();
		var gender = $('select').eq(0).val();
		var email = $('input').eq(2).val();
		var category = $('select').eq(1).val();
		var date_pledged = $('input').eq(3).val();
		var date_registered = $('input').eq(4).val();

		window.location.href=siteUrlExport+"?name="+name+"&birthdate="+birthdate+"&gender="+gender+"&email="+email+"&category="+category+"&date_registered="+date_registered+"&date_pledged="+date_pledged;
	};

		$scope.loadItems();

}]);