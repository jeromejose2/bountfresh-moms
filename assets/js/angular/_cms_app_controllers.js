var CMSAppControllers = angular.module('CMSAppControllers',['serviceControllers','angularFileUpload','ngDialog','textAngular']);

CMSAppControllers.controller('mainCtrl',['$rootScope','$scope','$route','$http','$resource','$window','ngDialog',function($rootScope,$scope,$route,$http,$resource,$window,ngDialog){

	$scope.statusDialog = null;
	$scope.notifyDialog = null;

	$rootScope.$on('$routeChangeStart',function(){

		$scope.openStatusDialog();

	});

 	$rootScope.$on('$routeChangeSuccess',function(){
		$rootScope.pageTitle = $route.current.title;
		$scope.statusDialog.close();

		$http.get('../api/cms/login_check').success(function(data){
			if(data.login_status=='failed'){
				$rootScope.login_status = false;
  				$window.location.href = '#/login';
			}else{
				$rootScope.login_status = true;
			}
		});

	});

	$scope.logout = function()
	{
 
		$http.get('../api/cms/logout').success(function(data){
			$rootScope.login_status = false;
  			$window.location.href = '#/login';			 
		});

	}

	$scope.openStatusDialog = function () {
        
        $scope.statusDialog = ngDialog.open({
								template: 'openStatusDialog',
								closeByDocument: false,
								closeByEscape: false,
								showClose:false
							});

    };

     $scope.openNotify = function (message) {
		
		$scope.notifyDialog = ngDialog.open({
								template: '<p>'+message+'</p>',
					 			plain: true
							}); 
	};


	
}]);


CMSAppControllers.controller('loginCtrl',['$rootScope','$scope','$http','$window',function($rootScope,$scope,$http,$window){

 	$scope.login = function()
	{

		var param = $scope.user;
		$scope.login_message = false;

		$http.post('../api/cms/login',param).success(function(data){

			console.log(data.login_message);

			if(data.logged_in==true){
				$rootScope.login_status=true;
				$window.location.href = '#/home';
			}else{
				$rootScope.login_status = false;
				$scope.login_message = data.login_message;
			}
		});

	}

	
 	 
}]); 


CMSAppControllers.controller('homeCtrl',['$scope','$http','Graph','Summary_Registrants','Summary_Comments','Summary_Registrants_Month','Summary_Comments_Month',function($scope,$http,Graph,Summary_Registrants,Summary_Comments,Summary_Registrants_Month,Summary_Comments_Month){

 	 $scope.chart = {};
 	 $scope.summaryRegistrants = [];
 	 $scope.summaryComments = [];
 	 $scope.summaryRegistrantsMonth = [];
 	 $scope.summaryCommentsMonth = [];

 	 $scope.loadItems = function() {

 	 	$scope.chart = Graph.get();
 	 	$scope.summaryRegistrants = Summary_Registrants.get();
 	 	$scope.summaryComments = Summary_Comments.get();
 	 	$scope.summaryRegistrantsMonth = Summary_Registrants_Month.get();
 	 	$scope.summaryCommentsMonth = Summary_Comments_Month.get();

 	 }

 	 $scope.loadItems();

}]);


CMSAppControllers.controller('recipesListCtrl',['$scope','$window','$resource','Recipe','$routeParams','$route',function($scope,$window,$resource,Recipe,$routeParams,$route){
 	
 	$scope.items = [];
 	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-recipes' : $routeParams.page;
 	$scope.page = $routeParams.page;

	$scope.deleteItem = function(item){
 		if(confirm('Are you sure you want to delete?')){ 
 			  item.$delete({id:item.id},function(data){
			  		 $scope.loadItems();			  	
			  }); 
		}		
	};

	$scope.loadItems = function(){
		$scope.items = Recipe.query({page:$routeParams.page});
	};

	$scope.loadItems();
	  
}]);

CMSAppControllers.controller('recipesAddCtrl',['$scope','$route','$http','Recipe','$window','$upload','$timeout','$routeParams',function($scope,$route,$http,Recipe,$window,$upload,$timeout,$routeParams){

	$scope.recipe = new Recipe();
	$scope.selectedFiles = [];
	$scope.assetBaseURL = '../assets/';
	$scope.apiBaseURL = '../api/';
	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
 	$scope.progress = -1;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-recipes' : $routeParams.page;
	$scope.recipe.page = $routeParams.page;
  
	$scope.addItem = function(){

		if($scope.recipe.image){

			$scope.recipe.$save(function(data){
 				$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
			});

		}
		
	}; 
	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);

		}else{
			$scope.errorUpload = 'The filetype you are attempting to upload is not allowed.';
			return;
		}



		$upload.upload({
				url : $scope.apiBaseURL+'upload/recipe',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;
				if(!response.error){
					$scope.recipe.image = response.new_image;
 				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});

	};

 
 		
}]);


CMSAppControllers.controller('recipesEditCtrl',['$route','$scope','Recipe','$routeParams','$window','$upload','$http','$timeout',function($route,$scope,Recipe,$routeParams,$window,$upload,$http,$timeout){

	$scope.recipe = new Recipe();
  	$scope.selectedFiles = false;
  	$scope.assetBaseURL = '../assets/';
  	$scope.apiBaseURL = '../api/';
  	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-recipes' : $routeParams.page;
	$scope.recipe.page = $routeParams.page;
  
  
	$scope.updateItem = function(){
		 
		$scope.recipe.$update(function(data){
 			$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
		});

  	};

	$scope.loadItem = function(){
		$scope.recipe = Recipe.get({id:$routeParams.id,page:$scope.page});
   	};

  


  	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);
		}else{
			$scope.errorUpload = 'The filetype you are attempting to upload is not allowed.';
			return;
		}

		$upload.upload({
				url : $scope.apiBaseURL+'upload/recipe',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;
				if(!response.error){
					$scope.recipe.image = response.new_image;
				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {
				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
				console.log($scope.progress);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});

	};

 
	$scope.loadItem();



}]);


CMSAppControllers.controller('articlesListCtrl',['$scope','$window','$resource','Article','$route','$routeParams',function($scope,$window,$resource,Article,$route,$routeParams){
 	
	$scope.items = [];
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-articles' : $routeParams.page;
	$scope.page = $routeParams.page;

	$scope.deleteItem = function(item){
 		if(confirm('Are you sure you want to delete?')){
			
		    item.$delete({id:item.id},function(){
				$scope.loadItems();
			});

		}		
	};

	$scope.loadItems = function()
	{
		$scope.items = Article.query({page:$routeParams.page});
	}

	$scope.loadItems();
	  
}]);

CMSAppControllers.controller('articlesAddCtrl',['$scope','$http','Article','$window','$upload','$timeout','$routeParams',function($scope,$http,Article,$window,$upload,$timeout,$routeParams){

	$scope.article = new Article();
	$scope.selectedFiles = [];
	$scope.assetBaseURL = '../assets/';
	$scope.apiBaseURL = '../api/';
	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-articles' : $routeParams.page;
	$scope.article.page = $routeParams.page;
 

  
	$scope.addItem = function(){

		if($scope.article.image){

			$scope.article.$save(function(data){
				$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
			});

		}
		
	};
 
 
	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);
		}


		$upload.upload({
				url : $scope.apiBaseURL+'upload/article',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;

 				if(!response.error){
					$scope.article.image = response.new_image;
				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});

	};
 

 
 		
}]);


CMSAppControllers.controller('articlesEditCtrl',['$scope','Article','$routeParams','$window','$upload','$http','$timeout',function($scope,Article,$routeParams,$window,$upload,$http,$timeout){

	$scope.article = new Article();
  	$scope.selectedFiles = false;
  	$scope.assetBaseURL = '../assets/';
  	$scope.apiBaseURL = '../api/';
  	$scope.preLoader = $scope.assetBaseURL+'img/loading.gif';
	$scope.errorUpload = false;
	$scope.state = $routeParams.page=='kitchen' ? 'kitchen-articles' : $routeParams.page;
	$scope.article.page = $routeParams.page;



	$scope.updateItem = function(){
		 
		$scope.article.$update(function(data){
			$window.location.href = '#/'+$scope.state+'/page/'+$routeParams.page;
		});

  
	};

	$scope.loadItem = function(){
		$scope.article = Article.get({id:$routeParams.id,page:$routeParams.page});		
  	};
 
   
  	$scope.onFileSelect = function($files){


		$scope.selectedFiles = $files;
		$scope.errorUpload = false;
		$scope.progress = 0;
		var i = 0;
		var $file = $files[i];
		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.dataUrl = e.target.result;
					});
				}
			}(fileReader, i);
		}


		$upload.upload({
				url : $scope.apiBaseURL+'upload/article',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) {
				var response = response.data;

				if(!response.error){
					$scope.article.image = response.new_image;
				}else
					$scope.errorUpload = response.error;

			}, null, function(evt) {
				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
				console.log($scope.progress);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});

	};

 	$scope.loadItem();
 
}]);

CMSAppControllers.controller('image_headersListCtrl',['$scope','$window','$resource','Image_header','$route',function($scope,$window,$resource,Image_header,$route){
 	
	$scope.items = [];
 

	$scope.deleteItem = function(item){
 		if(confirm('Are you sure you want to delete?')){
 
		    item.$delete({id:item.id},function(){
				$scope.loadItems();
			});
		}		
	};

	$scope.loadItems = function()
	{
		$scope.items = Image_header.query();
	};

	$scope.loadItems();

 	  
}]);


CMSAppControllers.controller('image_headersAddCtrl',['$scope','$window','$resource','Image_header','$route','$http','$timeout','$upload',function($scope,$window,$resource,Image_header,$route,$http,$timeout,$upload){
 	
 
	$scope.image_header = new Image_header();

 	$scope.assetBaseURL = '../assets/';
  	$scope.apiBaseURL = '../api/';
  	$scope.image_headers = [];

 

	$scope.abort = function(index) {
		$scope.upload[index].abort(); 
		$scope.upload[index] = null;
	};

	$scope.onFileSelect = function($files)
	{
		$scope.selectedFiles = $files;
		$scope.upload = [];		
		$scope.dataUrls = [];
		$scope.progress = [];

		if ($scope.upload && $scope.upload.length > 0) {
			for (var i = 0; i < $scope.upload.length; i++) {
				if ($scope.upload[i] != null) {
					$scope.upload[i].abort();
				}
			}
		}

		for ( var i = 0; i < $files.length; i++) {
			var $file = $files[i];
			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.dataUrls[index] = e.target.result;
						});
					}
				}(fileReader, i);
			}
			$scope.progress[i] = -1;
 			$scope.start(i);
		}


	};

	$scope.start = function(index) {

		$scope.progress[index] = 0;	

		$scope.upload[index] = $upload.upload({
			url : $scope.apiBaseURL+'upload/image_header',
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			file: $scope.selectedFiles[index],
			fileFormDataName: 'image'
		}).then(function(response) {
			var response = response.data;
			
			if(!response.error){
				$scope.image_headers[index] = response.new_image;
			}else{
				$scope.errorUpload[index] = response.error;
			}
 
		}, null, function(evt) {
			$scope.progress[index] = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	}


	$scope.remove = function(index) {

		var param = {image:$scope.image_headers[index]};

  		$http({url: $scope.apiBaseURL+'upload/remove_header',params:param,method:'get'})
		.success(function(response){
			console.log(response);
		});
		 
		$scope.selectedFiles.splice(index, 1);
		$scope.dataUrls.splice(index, 1);
		$scope.progress.splice(index, 1);
		$scope.errorUpload.splice(index, 1);
 
	}


	$scope.addItem = function()
	{
		$scope.image_header.images = $scope.image_headers;
		$scope.image_header.$save(function(data){
			$window.location.href = '#/image_headers';
		});
	}
 
	  
}]);

CMSAppControllers.controller('registrantsListCtrl',['$scope','Registrant',function($scope,Registrant){

	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Registrant.query();
 	}

	$scope.deleteItem = function(item){

 		if(confirm('Are you sure you want to delete?')){
			
		    item.$delete({id:item.id},function(){
				$scope.loadItems();
			});

		}		
	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('privacyPolicyListCtrl',['$scope','Privacy_policy',function($scope,Privacy_policy){

	$scope.privacy_policy = new Privacy_policy();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.privacy_policy = Privacy_policy.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.privacy_policy.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('termsAndConditionsListCtrl',['$scope','Terms_and_conditions',function($scope,Terms_and_conditions){

	$scope.terms_and_conditions = new Terms_and_conditions();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.terms_and_conditions = Terms_and_conditions.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.terms_and_conditions.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('bountyFreshFoodsListCtrl',['$scope','Bounty_fresh_foods',function($scope,Bounty_fresh_foods){

	$scope.bounty_fresh_foods = new Bounty_fresh_foods();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.bounty_fresh_foods = Bounty_fresh_foods.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.bounty_fresh_foods.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('bountyAgroVenturesListCtrl',['$scope','Bounty_agro_ventures',function($scope,Bounty_agro_ventures){

	$scope.bounty_agro_ventures = new Bounty_agro_ventures();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.bounty_agro_ventures = Bounty_agro_ventures.get({id:2});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.bounty_agro_ventures.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);

CMSAppControllers.controller('faqEditCtrl',['$scope','FAQ',function($scope,FAQ){

 
	$scope.faq = new FAQ();
 	$scope.edit_mode = false;


 	$scope.loadItem = function(){
		$scope.faq = FAQ.get({id:1});		
  	};

 	$scope.editItem = function(){
		$scope.edit_mode = true;
 	}

 	$scope.cancelEdit = function(){
 		$scope.edit_mode = false;
 	}

 	$scope.updateItem = function(){
		
		$scope.openStatusDialog();

		$scope.faq.$update(function(data){
			$scope.cancelEdit();
			$scope.loadItem();
			$scope.statusDialog.close();
 		});

  
	};

	$scope.loadItem();
	 


}]);


CMSAppControllers.controller('recipeCommentListCtrl',['$scope','Recipe_Comment',function($scope,Recipe_Comment){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Recipe_Comment.query();
 	}

	 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		if(confirm('Are you sure you want to approve?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		if(confirm('Are you sure you want to disapprove?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		if(confirm('Are you sure you want to make it pending?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('recipeCommentRepliesListCtrl',['$scope','Recipe_Comment_Replies',function($scope,Recipe_Comment_Replies){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Recipe_Comment_Replies.query();
 	}

	 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		if(confirm('Are you sure you want to approve?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		if(confirm('Are you sure you want to disapprove?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		if(confirm('Are you sure you want to make it pending?')){
			item.$update(function(data){
				 $scope.openNotify(data.message);
				 $scope.loadItems();
	 		});
	 	}


	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('articleCommentListCtrl',['$scope','Article_Comment',function($scope,Article_Comment){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Article_Comment.query();
 	}
 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		item.$update(function(data){
			 $scope.openNotify(data.message);
			 $scope.loadItems();
 		});

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.loadItems();


}]);

CMSAppControllers.controller('articleCommentRepliesListCtrl',['$scope','Article_Comment_Replies',function($scope,Article_Comment_Replies){


	$scope.items = {};

	$scope.loadItems = function()
	{
		$scope.items = Article_Comment_Replies.query();
 	}
 

	$scope.approve = function(item){

		item['approval_status'] = '1';

		item.$update(function(data){
			 $scope.openNotify(data.message);
			 $scope.loadItems();
 		});

	};

	$scope.disapprove = function(item){

		item['approval_status'] = '2';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.pending = function(item){

		item['approval_status'] = '0';
		
		item.$update(function(data){
			$scope.openNotify(data.message);
			$scope.loadItems();
 		});

	};

	$scope.loadItems();


}]);