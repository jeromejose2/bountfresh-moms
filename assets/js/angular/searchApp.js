'use strict';

angular.module('articleApp',['googleSearch','Authentication'])
.controller('googleSearch',['$scope','$location','$http',function($scope,$location,$http){

	function searchGoogle = function(){

		var source = $location.absUrl(),arr = source.split('/'),searchTerm=arr[arr.length - 2];

		$scope.googleStuff = {};
		$scope.google_query = true;
		$scope.result_message = 'No results found.';

	 
	 	host = 'https://www.googleapis.com/customsearch/v1',
		args = {
		'version': '1.0',
		'searchTerm': searchTerm,
		'results': '8',
		'callback': 'JSON_CALLBACK',
		'key':'AIzaSyDYjFcQuQsrGP4GeowZyG_Piy0L-Soo2H4',
		'cx' : '013736079760604649161:tzj7cy2cday'
		},
		params = ('?key=' + args.key + '&cx='+args.cx+'&q=' + args.searchTerm + '&callback=' + args.callback),
		httpPromise = $http.jsonp(host + params);

		httpPromise.then(function (response) {
	 		$scope.googleStuff = response.data.items;
			$scope.google_query = false;
		}, function (error) {
			$scope.result_message = 'Something went wrong. Please try again.';
		});

	}

	$scope.searchGoogle();
	
 	
}]);
