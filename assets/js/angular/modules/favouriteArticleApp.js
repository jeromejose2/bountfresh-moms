angular.module('favoriteArticleApp',['ngDialog','socialShare','Authentication'])
.controller('favouriteArticleCtrl',['$scope','$http','ngDialog','socialShare','Auth','$window',function($scope,$http,ngDialog,socialShare,Auth,$window){

	$scope.items = {};


	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
				$scope.my_account = 1;
			}else{
				$window.location.href="login";
			}

		});
	}

	$scope.logout = function(){
		Auth.logout().success(function(){
			$window.location.href = "login";
		});
	}

	$scope.openConfirm = function (id) {

		$scope.removeValue = id;
		ngDialog.openConfirm({
			template: 'modalDialogId',
			className: 'ngdialog-theme-default'
		}).then(function (value) {
			$scope.removeFavorite();
		}, function (reason) {
			 
		});

	};


	$scope.shareFB = function(id,page){
 		socialShare.FBSharePage(id,page,FB);
	};
	$scope.shareTwitter = function(id,page){
 		socialShare.TwitterSharePage(id,page);
	};
	$scope.sharePintirest = function(id,page){
 		socialShare.PinterestSharePage(id,page);
	};

	$scope.dialog_open = function(message,showClose,closeByDocument,closeByEscape){

		$scope.dialog = ngDialog.open({
								template: '<center>'+message+'</center>',
					 			plain: true,
					 			showClose:showClose,
					 			closeByDocument: closeByDocument,
								closeByEscape: closeByEscape
							});
	}

	$scope.dialog_close = function(){

		$scope.dialog.close();
	}

	$scope.loadItems = function(){

		$http.get('api/bounty/favorite_articles').success(function(data){
			$scope.items = data;
		});

	}
	 

	$scope.removeFavorite = function(){

		var param = {id:$scope.removeValue};
  
		$http.post('api/bounty/remove_favourite_article',param).success(function(response){
			 $scope.loadItems();
			 $scope.dialog_open(response.message,true,true,true);
			 
		});

	}
 
	$scope.auth();
	$scope.loadItems();
 	 

}]);