'use strict';

angular.module('recipeApp',['googleSearch','socialShare','ngDialog','ngSanitize','Authentication'])
.controller('recipeCtrl',['$scope','$http','socialShare','ngDialog','$location','Auth',function($scope,$http,socialShare,ngDialog,$location,Auth){
	$scope.recipe = {};
	$scope.recipe_comments = null;
	$scope.prev_recipe = null;
	$scope.next_recipe = null;
	$scope.parent_id = 0;
	$scope.user = {};
	$scope.replies = {};

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
			}

		});
	}

	$scope.shareFB = function(id,page){
 		socialShare.FBSharePage(id,page,FB);
	};
	$scope.shareTwitter = function(id,page){
 		socialShare.TwitterSharePage(id,page);
	};
	$scope.sharePintirest = function(id,page){
 		socialShare.PinterestSharePage(id,page);
	};

	$scope.dialog_open = function(message,showClose,closeByDocument,closeByEscape){

		$scope.dialog = ngDialog.open({
								template: '<center>'+message+'</center>',
					 			plain: true,
					 			showClose:showClose,
					 			closeByDocument: closeByDocument,
								closeByEscape: closeByEscape
							});
	}

	$scope.dialog_close = function(){

		$scope.dialog.close();
	}

	$scope.loadRecipe = function(){

		var source = $location.absUrl(),arr = source.split('/'),id=arr[arr.length - 2];

		$scope.page_title = arr[arr.length-1];
		$scope.dialog_open('Please wait...');

		$http.get('api/bounty/recipe?id='+id).success(function(data){
			$scope.recipe = data.recipe;
			$scope.prev_recipe = data.previous_recipe;
			$scope.next_recipe = data.next_recipe;
			$scope.loadComments();
			$scope.dialog_close();
 		});

	};

	$scope.addFavourite = function(){

		var id = $scope.recipe.id;

		$http.post('api/bounty/favourite_recipe',{id:id}).success(function(response){

			$scope.dialog_open(response.message,true);

		});

	};

	$scope.addComment = function(parent_id){

		var param = {id:$scope.recipe.id,parent_id:parent_id,comment:$scope.recipe.comment,comment_reply:$scope.recipe.comment_reply};

 		if(!$scope.recipe.comment && !$scope.recipe.comment_reply){
			$scope.dialog_open('Write your comment to continue.',true,true,true);
			return false;
		}

		$scope.dialog_open('Please wait...');

 		$http.post('api/bounty/recipe_comments',param).success(function(response){
 			$scope.recipe.comment = '';
 			$scope.recipe.comment_reply = '';
 			$scope.dialog_close();
 			$scope.dialog_open(response.message,true,true,true);

		});

	};

	$scope.replies_show = function(parent_id){

		var new_scope = 'subject'+parent_id;
		$scope.show_reply = [];

		$scope[new_scope] = true;
		$scope.show_reply[parent_id] = true;

		$http.get('api/bounty/recipe_comment_replies?id='+parent_id).success(function(data){

 			$scope.replies[parent_id] = data;

		});


	};

	$scope.loadComments = function(){

		$http.get('api/bounty/recipe_comments?id='+$scope.recipe.id).success(function(response){

			$scope.recipe_comments = response;

		});

	};

	$scope.userDetails = function()
	{
	 	$http.get('api/bounty/user').success(function(data){
			$scope.user = data;
 		});

	};

	$scope.auth();
	$scope.loadRecipe();
	$scope.userDetails();

}]);
