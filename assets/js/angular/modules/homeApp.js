'use strict';

angular.module('homeApp',['imageSlider','googleSearch','socialShare','Authentication'])
.controller('homeCrtl',['$scope','$http','socialShare','Auth','$window', '$rootScope', function($scope,$http,socialShare,Auth,$window,$rootScope){

	$rootScope.aw = "AW"


	$scope.featured_recipes = [];
	$scope.featured_articles = [];
	$scope.header_images = [];

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
			}

		});
	}

	$scope.logout = function(){
		Auth.logout().success(function(data){
			$window.location.href = "login";
		});
	}

	$scope.loadItems = function(){

		$http.get('api/bounty/header_images?page=1&width=1024&height=501').success(function(data){
			$scope.header_images = data;
 		});

		$http.get('api/bounty/recipes_home').success(function(data){
			$scope.featured_recipes = data;
		});

		$http.get('api/bounty/articles_home').success(function(data){
			$scope.featured_articles = data;
		});

	};

	$scope.shareFB = function(id,page){
		socialShare.FBSharePage(id,page,FB);
	};
	$scope.shareTwitter = function(id,page){
 		socialShare.TwitterSharePage(id,page);
	};
	$scope.sharePintirest = function(id,page){

 		socialShare.PinterestSharePage(id,page);
	};

	$scope.loadItems();
	$scope.auth();

	console.log("CALL HOMEAPP");

}])

.directive("smartBanner",function(){
    return {
        restrict: "E",
        template: '<meta property="og:title" content=""></meta>',
        replace: true,
        link: function(scope) {
         var metaTag=document.createElement('meta');
          metaTag.property = "og:title";
          metaTag.content = "TEST_DIRECTIVES";
          document.getElementsByTagName('head')[0].appendChild(metaTag);
        }
   }
});