'use strict';

angular.module('masterBedroomApp',['imageSlider','googleSearch','topPicks','socialShare','Authentication'])
.controller('masterBedroomCtrl',['$scope','$http','socialShare','Auth','$window',function($scope,$http,socialShare,Auth,$window){
	
	$scope.masterBedroomNav = 'active';
	$scope.top_picks = [];
	$scope.articles = [];
	$scope.header_images = [];
	$scope.articles = {};
 	$scope.articleLength = 0;
 	$scope.articleLimit = 4;
 	$scope.articleCurrentPage = 1;

 	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
			}

		});
	}

	$scope.logout = function(){
		Auth.logout().success(function(data){
			$window.location.href = "login";
		});
	}


 	$scope.shareFB = function(id,page){
 		socialShare.FBSharePage(id,page,FB);
	};
	$scope.shareTwitter = function(id,page){
 		socialShare.TwitterSharePage(id,page);
	};
	$scope.sharePintirest = function(id,page){
 		socialShare.PinterestSharePage(id,page);
	};

	$scope.loadItems = function(){		 

		$http.get('api/bounty/header_images?page=5&width=1024&height=342').success(function(data){
			$scope.header_images = data;
  		});
 
		$http.get('api/bounty/master_bedroom_room_articles').success(function(data){
			$scope.articles = data;
			$scope.articleLength = data.length;
		});

		$http.get('api/bounty/master_bedroom_top_picked_articles').success(function(data){
			$scope.top_picks = data;
 		});
		 
	};

	$scope.showMoreArticles = function(){

    	$scope.articleCurrentPage +=1;
    	$scope.articleLimit *= $scope.articleCurrentPage;

    };

    $scope.auth();
	$scope.loadItems();

}]);