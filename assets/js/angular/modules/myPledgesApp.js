(function(){
    
    'use strict';
 
angular
    
    .module('myPledgesApp', ['Authentication','PostSharing'])

    .controller('myPledgesCtrl',['$location','$scope','Auth','$window','$http','$rootScope','postSharing', 
        function($location,$scope,Auth,$window,$http,$rootScope,postSharing){

        $rootScope.onMyAccount = "TESTING";

        $scope.pledges = [];
            $scope.getData = function(){
                $http.get('api/bounty/pledge_list').success(function(data){
                        $scope.pledges  = data;
                    }); 
            }

        $scope.getData();
        
     //show more functionality
            var pagesShown = 1;
            var pageSize = 3;
            
            $scope.paginationLimit = function(data) {
                return pageSize * pagesShown;
            };
            $scope.hasMoreItemsToShow = function() {
                return pagesShown < ($scope.pledges.length / pageSize);
            };
            $scope.showMoreItems = function() {
                pagesShown = pagesShown + 1;       
            };

        $scope.auth = function(){
            Auth.load().success(function(data){
                if(!data.error){
                    $scope.logged = 1;
                    $scope.my_account = 1;
                }else{
                    $window.location.href="login";
                }
            });
        }

        $scope.getLocationPath = function(){
            return $location.path();
        }

        $scope.logout = function(){
            Auth.logout().success(function(){
                $window.location.href = "login";
            });
        }
        $scope.auth();  

        $scope.openTwitter = function(type,message,category){
            var message = (message !=null ) ? message : '';

            postSharing.postTwitter(type,message,category);
        }
        
        $scope.openFacebook = function(type,message,category){
            var message = (message !=null ) ? message : '';
            
            postSharing.postFacebook(type,message,category);
        }
    }])

 
})(); 