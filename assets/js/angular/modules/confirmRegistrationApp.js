angular.module('confirmRegistrationApp',['googleSearch','ngDialog','Authentication'])
.controller('confirmRegistrationCtrl',['$scope','$window','$http','$location','ngDialog','Auth',function($scope,$window,$http,$location,ngDialog,Auth){
	$scope.user = {};

	$scope.dialog_open = function(message,showClose,closeByDocument,closeByEscape){

		$scope.dialog = ngDialog.open({
								template: '<center>'+message+'</center>',
					 			plain: true,
					 			showClose:showClose,
					 			closeByDocument: closeByDocument,
								closeByEscape: closeByEscape
							});
	}

	$scope.dialog_close = function(){
		$scope.dialog.close();
	}

	$scope.login = function(isValid)
	{
		if(isValid){

			var inputs = $scope.user;
 
			Auth.login(inputs).success(function(data){

				if(data.error){
					$scope.login_message = data.message;
					$scope.user = {};
				}else{
					$window.location.href = 'my-account';
				}

			});


		}
		

	};

	$scope.confirmRegistration = function(){
		var source = $location.absUrl(),arr = source.split('/'),id=arr[arr.length - 2],token = arr[arr.length-1];

		$http.post('api/bounty/confirm_registration',{token:token}).success(function(data){

			$scope.dialog_open(data.message,true,true,true);

		});

	};

	$scope.confirmRegistration();


}]);