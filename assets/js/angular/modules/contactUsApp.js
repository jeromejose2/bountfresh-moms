angular.module('contactUsApp',['googleSearch','ngDialog'])
.controller('contactUsAppCtrl',['$scope','$http','ngDialog','$sce',function($scope,$http,ngDialog,$sce){

	$scope.user = {};
	$scope.details_bounty_fresh_foods = {};
	$scope.details_bounty_agro_ventures = {};

	$scope.loadItems = function() {
		$http.get('api/bounty/contact_details_bff').success(function(data) {
			$scope.details_bounty_fresh_foods = $sce.trustAsHtml(data.content);
		});

		$http.get('api/bounty/contact_details_bav').success(function(data) {
			$scope.details_bounty_agro_ventures = $sce.trustAsHtml(data.content);
		});
	}

	$scope.dialog_open = function(message,showClose,closeByDocument,closeByEscape){

		$scope.dialog = ngDialog.open({
								template: '<center>'+message+'</center>',
					 			plain: true,
					 			showClose:showClose,
					 			closeByDocument: closeByDocument,
								closeByEscape: closeByEscape
							});
	}

	$scope.dialog_close = function(){

		$scope.dialog.close();
	}
		
	$scope.submitInquiry = function()
	{

		$scope.dialog_open('Please wait...');
		$http.post('api/bounty/send_contact_us_form',$scope.user).success(function(data){

			$scope.dialog_close();

			if(data.error){
				
				$scope.dialog_open(data.message,true,true,true);

			}else{

				$scope.user = {};
				$scope.dialog_open(data.message,true,true,true);

			}
			
		});
	}

	$scope.loadItems();

}]);