angular.module('kitchenApp',['imageSlider','googleSearch','topPicks','socialShare','Authentication'])
.controller('kitchenCrtl',['$scope','$http','socialShare','Auth','$window',function($scope,$http,socialShare,Auth,$window){

	$scope.kitchenNav = 'active';
	$scope.recipes = [];
    $scope.recipeLength = 0;
    $scope.recipeLimit = 4;
    $scope.recipeCurrentPage = 1;

    $scope.meal_types = [];
    $scope.meal_budgets = []; 
 
 	$scope.articles = {};
 	$scope.articleLength = 0;
 	$scope.articleLimit = 4;
 	$scope.articleCurrentPage = 1;
	$scope.header_images = {};

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
			}

		});
	}

	$scope.logout = function(){
		Auth.logout().success(function(data){
			$window.location.href = "login";
		});
	}

	$scope.shareFB = function(id,page){
 		socialShare.FBSharePage(id,page,FB);
	};
	$scope.shareTwitter = function(id,page){
 		socialShare.TwitterSharePage(id,page);
	};
	$scope.sharePintirest = function(id,page){
 		socialShare.PinterestSharePage(id,page);
	};
 
	$scope.loadItems = function(){

		$http.get('api/bounty/header_images?page=2&width=1024&height=342').success(function(data){
			$scope.header_images = data;
		});

		$http.get('api/bounty/kitchen_recipes').success(function(data){
			$scope.recipes = data;
			$scope.recipeLength = data.length;
		});

		$http.get('api/bounty/kitchen_articles').success(function(data){
			$scope.articles = data;
			$scope.articleLength = data.length;
  		});

		$http.get('api/bounty/kitchen_top_picked_articles').success(function(data){
			$scope.top_picks = data;
		});

	};

	$scope.showMoreRecipes = function(){

    	$scope.recipeCurrentPage += 1;
    	$scope.recipeLimit *= $scope.recipeCurrentPage;

    };

    $scope.showMoreArticles = function(){

    	$scope.articleCurrentPage +=1;
    	$scope.articleLimit *= $scope.articleCurrentPage;

    };

    $scope.mealTypes = function(){

    	$http.get('api/bounty/meal_types').success(function(data){
			$scope.meal_types = data;
		});

    };

    $scope.mealBudgets = function(){

    	$http.get('api/bounty/meal_budgets').success(function(data){
			$scope.meal_budgets = data;
		});

    };

    $scope.auth();
	$scope.loadItems();
	$scope.mealTypes();   
	$scope.mealBudgets();

}]);