angular.module('myAccountApp',['angularFileUpload','ngDialog','Authentication'])
.controller('myAccountCtrl',['$scope','$http','$upload','$timeout','$location','ngDialog','Auth','$window',function($scope,$http,$upload,$timeout,$location,ngDialog,Auth,$window){

	$scope.edit_mode = false;
 	$scope.user = {};
 	$scope.selectedFiles = [];
	$scope.progress = -1;
	$scope.userImage = false;
 	$scope.months = [{val:1,name:'Jan'},{val:2,name:'Feb'},{val:3,name:'Mar'},{val:4,name:'Apr'},
      				 {val:5,name:'May'},{val:6,name:'Jun'},{val:7,name:'Jul'},{val:8,name:'Aug'},
      				 {val:9,name:'Sep'},{val:10,name:'Oct'},{val:11,name:'Nov'},{val:12,name:'Dec'}];


    $scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
				$scope.my_account = 1;
			}else{
				$window.location.href="login";
			}

		});
	}

	$scope.logout = function(){
		Auth.logout().success(function(data){
			$window.location.href = "login";
		});
	}

    $scope.dialog_open = function(message,showClose,closeByDocument,closeByEscape){

		$scope.dialog = ngDialog.open({
								template: '<center>'+message+'</center>',
					 			plain: true,
					 			showClose:showClose,
					 			closeByDocument: closeByDocument,
								closeByEscape: closeByEscape
							});
	}

	$scope.dialog_close = function(){

		$scope.dialog.close();
	}
  
	
	$scope.userDetails = function(){

	 	$http.get('api/bounty/user').success(function(data){
			$scope.user = data;
			$scope.userImage = data.edit_profile_photo;
 			$scope.getProvinces();			
 		});

	 }
 

    $scope.birthYears = function() {
      	var dateTime = new Date();
      	var start = dateTime.getFullYear() - 90;
      	var end = dateTime.getFullYear() - 18;
      	var years = [];
      	var i = 0;

      	while(start<=end){
      		years[i] = start;
       		start++;
      		i++;
      	}

      	$scope.years = years;
  
    }

    $scope.birthDays = function () {
	  var num = 31;
	  var i;
	  var days = [];
	  for (i = 1; i <= num; i++) {days.push(String(i)); }
	  return days;
	};

	$scope.updateDetails = function(isValid){

		if(isValid){

			$scope.dialog_open('Please wait...');

			$http.post('api/bounty/update_profile_details',$scope.user).success(function(response){
 				$scope.dialog_close();
				$scope.dialog_open(response.message,true,true,true);
				if(response.error == 0){
					setTimeout(function(){ 
						location.reload();
					}, 2000);
				}
			});

		}
		

	};

	$scope.onFileSelect = function($files){

			$scope.selectedFiles = $files;
	 		$scope.progress = 0;
			var i = 0;
			var $file = $files[i];

			if (window.FileReader && $file.type.indexOf('image') > -1) {
				var fileReader = new FileReader();
				fileReader.readAsDataURL($files[i]);
				var loadFile = function(fileReader, index) {
					fileReader.onload = function(e) {
						$timeout(function() {
							$scope.userImage = e.target.result;
						});
					}
				}(fileReader, i);
			}


			$upload.upload({
				url : 'api/bounty/upload_profile',
				method: 'POST',
				headers: {'my-header': 'my-header-value'},
				file: $scope.selectedFiles[i],
				fileFormDataName: 'image'
			}).then(function(response) { 
				 
				var response = response.data;
				if(!response.error){
					$scope.user.image = response.new_image;
 					$scope.error = '';
 				}else
					$scope.error = response.error;

				$scope.progress = -1;				 

			}, null, function(evt) {				
				$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
			}).xhr(function(xhr){
				xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
			});

	};

	$scope.getMunicipal = function(){

		$http.get('api/bounty/cities?province='+$scope.user.province_id.province_id+'&city_id='+$scope.user.city_id).success(function(data){
        	$scope.cities = data.cities;
        	$scope.user.city_id = data.cities[data.index];
         	$scope.is_cities = true;
         });

	};

	$scope.getProvinces = function(){
 	 		
    	$http.get('api/bounty/provinces?province_id='+$scope.user.province_id).success(function(data){
    		 
        	 $scope.provinces = data.provinces;
        	 $scope.user.province_id = data.provinces[data.index];
         	 $scope.is_cities = false;
        	 $scope.getMunicipal();       	 
        });

    };
    
    $scope.auth();
    $scope.userDetails();
	$scope.birthYears();
	
}])
.directive('numbersOnly', function () {
    return  {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
        	var val = '';
            elm.on('keydown', function (event) {

            	 val = elm.val();
 
                 if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // backspace, enter, escape, arrows
                    // if(elm.val().length==6){
                    // 	elm.val(val.replace('-',''));
                    // }
                    return true;
                } else if (event.which >= 48 && event.which <= 57) {
                    // numbers
                    // if(elm.val().length==4){
                    // 	elm.val(val+'-');
                    // }

                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // numpad number
                    // if(elm.val().length==6){
                    // 	elm.val(val.replace('-',''));
                    // }
                    return true;
                } else if ([110, 190].indexOf(event.which) > -1) {
                    // dot and numpad dot
                    return false;
                }else {
                    event.preventDefault();
                    return false;
                }
            });
        }
    }
});