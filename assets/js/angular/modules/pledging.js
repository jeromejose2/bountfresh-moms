
// create our angular app and inject ngAnimate and ui-router
// =============================================================================
angular.module('pledingnApp', ['ngAnimate', 'ui.router', 'Authentication','PostSharing'])

// configuring our routes
// =============================================================================
.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider

        // route to show our basic form (/form)
        .state('#', {
            url: '/',
            templateUrl: 'partials/pledging_form.html'
        })

        // nested states
        // each of these sections will have their own view)

        .state('#/form2', {
            url: '/form2',
            templateUrl: 'partials/pledging_form2.html'
        })

        .state('#/success', {
            url: '/success',
            templateUrl: 'partials/pledging_success.html'
        })

    // catch all route
    // send users to the form page
    $urlRouterProvider.otherwise('/');
})


.directive('focus', ["$compile","$rootScope", function ( $compile, $rootScope ) {
  return {
        restrict: 'A',
      scope: '=',
      link : function(scope, elem, attr ){
            scope.$watch( function () {
            if($rootScope.hasSelection===true){
              elem[0].focus();
            }
          });
            elem.bind("blur",function(){
            $rootScope.hasSelection = false;
          });
      }
  }
}] )

.factory('catagories',function(){
    var catagories = {};
    catagories.cast = [
    {
        text: "Buhay"
    },
    {
        text: "Bahay"
    },
    {
        text: "Pamilya"
    },
    {
        text: "Kusina"
    }

    ];
    return catagories;
})
.factory('buhay',function(){
    var buhay = {};
    buhay.cast = [
    {
        text: "Me-Time"
    },
    {
        text: "School Days"
    },
    {
        text: "Holidays, Weekends, and Vacations"
    }
    ];
    return buhay;
})
.factory('bahay',function(){
    var bahay = {};
    bahay.cast = [
    {
        text: "Our Home"
    },
    {
        text: "Our Lifestyle"
    },
    {
        text: "Our Time at Home"
    }
    ];
    return bahay;
})
.factory('pamilya',function(){
    var pamilya = {};
    pamilya.cast = [
    {
        text: "Family Time"
    },
    {
        text: "Meal Times"
    },
    {
        text: "Special Occasions"
    }
    ];
    return pamilya;
})
.factory('kusina',function(){
    var kusina = {};
    kusina.cast = [
    {
        text: "my family’s favorite Fried Chicken"
    },
    {
        text: "my family’s favorite Baked Chicken"
    },
    {
        text: "my family’s favorite Roast Chicken"
    },
    {
        text: "my family’s favorite Stewed Chicken"
    }
    ];
    return kusina;
})
.factory('setNulls',function(){
    var setNulls = {};
    setNulls.cast = [
    {
        value: "00",
        text: "Select category from previous field"
    }
    ];
    return setNulls;
})

// our controller for the form
// =============================================================================
.controller('pledgingCtrl',[ '$location','$scope','Auth','$window','buhay','bahay','pamilya','kusina','setNulls','catagories','$http','$rootScope','postSharing', 
    function($location,$scope,Auth,$window,buhay,bahay,pamilya,kusina,setNulls,catagories,$http, $rootScope,postSharing) {
    $scope.catagories = catagories;
    $scope.types = setNulls;
    $scope.user = {};
    $rootScope.hasSelection = false;
    $scope.text2 = '';


    var vm = this;
        vm.globalData = {};

        if(!vm.globalData['firstStepLabel1']) vm.globalData.firstStepLabel1 = null;
        if(!vm.globalData['firstStepLabel2']) vm.globalData.firstStepLabel2 = null;
        if(!vm.globalData['commentBox']) vm.globalData.commentBox = null;
        if(!vm.globalData['secondStepLabel1']) vm.globalData.secondStepLabel1 = null;


    $scope.getData = function (){
        return vm.globalData;
    }

    $scope.changeData = function(value) {
        // if($scope.itemsuper.text == "Buhay") {
        //     $scope.types = buhay;
        // } else if($scope.itemsuper.text == "Bahay") {
        //     $scope.types = bahay;
        // } else if($scope.itemsuper.text == "Pamilya") {
        //     $scope.types = pamilya;
        // } else if($scope.itemsuper.text == "Kusina") {
        //     $scope.types = kusina;
        // } else {
        //     $scope.types = setNulls;
        // }

        if(value == "Buhay") {
            $scope.types = buhay;
        } else if(value == "Bahay") {
            $scope.types = bahay;
        } else if(value == "Pamilya") {
            $scope.types = pamilya;
        } else if(value == "Kusina") {
            $scope.types = kusina;
        } else {
            $scope.types = setNulls;
        }

        vm.globalData.firstStepLabel1 = value;
    } //end of changedata

    $scope.update = function(value) {
        vm.globalData.firstStepLabel2 = value;
        $scope.text3 = "Tell us exactly how you want to put your special touch on "+value;
        $rootScope.hasSelection = true;
    }

    $scope.auth = function(){
        Auth.load().success(function(data){
            if(!data.error){
                $scope.logged = 1;
                $scope.my_account = 1;
            }else{
                $window.location.href="pledge";
            }
        });
    }

    $scope.getLocationPath = function(){
        return $location.path();
    }

    $scope.logout = function(){
        Auth.logout().success(function(){
            $window.location.href = "pledge";
        });
    }
    $scope.auth();

    $scope.step1 = function(){
        //if you want to get all data
        var strReplace = $location.absUrl().replace('form2','/');
        window.location.href = strReplace;

    }

    $scope.step2 = function(commentBox){
        
        if($scope.getData().commentBox == null)
            $scope.getData().commentBox = commentBox;

        if( $scope.getData().firstStepLabel1 != null &&
            $scope.getData().firstStepLabel2 != null ){

            if($location.absUrl().indexOf('success')==-1){
                window.location.href = $location.absUrl() + 'form2';
            }else{
                var strReplace = $location.absUrl().replace('success','form2');
                window.location.href = strReplace;
            }
        }else{
            console.log("NO");
        }
        if($scope.getData().commentBox == null)
            $scope.getData().commentBox = 'special';
        else
            $scope.getData().commentBox = 'special by ' + commentBox;
        
        $scope.text1 = $scope.getData().firstStepLabel2;
        $scope.text2 = $scope.getData().commentBox;
    }

    $scope.step3 = function(nickname){
        if(nickname){
            $scope.getData().secondStepLabel1 = nickname;
            var strReplace = $location.absUrl().replace('form2','success');
            window.location.href = strReplace;
            //$http
        } else {
           console.log("Please input nickname");
        }
    }

    $scope.step4 = function(nickname){
        //http post here !!
        $window.location.href = 'my-pledges';
    }

    $scope.checkAllData = function(){
        console.log($scope.getData());
    }

    $scope.pledge = function(isValid)
    {

        if(isValid){

            $http.post('api/bounty/pledge',$scope.user).success(function(response){

                var callback = null;

                if(response.error){
                    $window.location.href = 'pledging#';
                    $scope.error_message = response.message;
                    $scope.user = {};
                }else{
                    callback = function() {
                        $scope.text1 = $scope.getData().firstStepLabel1;
                        $scope.text2 = $scope.getData().firstStepLabel2;
                        $scope.text3 = $scope.getData().commentBox;

                        $window.location.href = 'pledging#/success';
                    };
                    callback();
                }
            });
        }
    }

    $scope.openTwitter = function(type,message,category){
        postSharing.postTwitter(type,message,category);
    }
    
    $scope.openFacebook = function(type,message,category){
        postSharing.postFacebook(type,message,category);
    }
}]);

