'use strict';

angular.module('searchApp',['googleSearch','Authentication'])
.controller('searchCtrl',['$rootScope','$scope','$location','$http','Auth',function($rootScope,$scope,$location,$http,Auth){

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
			}

		});
	}

	$scope.queryGoogle = function(){

		var source = $location.absUrl(),arr = source.split('/'),searchTerm=arr[arr.length - 1];

		searchTerm = searchTerm.replace(/-/g, " ")

		console.log(searchTerm);

		$scope.googleStuff = {};
		$scope.google_query = true;
		$scope.result_message = 'No results found.';
		$rootScope.searchkey = searchTerm;

	 	var host = 'https://www.googleapis.com/customsearch/v1';
		var args = {
		'version': '1.0',
		'searchTerm': searchTerm,
		'results': '8',
		'callback': 'JSON_CALLBACK',
		'key':'AIzaSyDYjFcQuQsrGP4GeowZyG_Piy0L-Soo2H4',
		'cx' : '013736079760604649161:tzj7cy2cday'
		};
		var params = ('?key=' + args.key + '&cx='+args.cx+'&q=' + args.searchTerm + '&callback=' + args.callback);
		var httpPromise = $http.jsonp(host + params);

		httpPromise.then(function (response) {
	 		$scope.googleStuff = response.data.items;
			$scope.google_query = false;
		}, function (error) {
			$scope.result_message = 'Something went wrong. Please try again.';
		});

	}

	$scope.queryGoogle();


}]);
