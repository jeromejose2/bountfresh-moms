angular.module('resetPasswordApp',['Authentication','ngDialog'])
.controller('resetPasswordCtrl',['$scope','$http','Auth','ngDialog','$window','$location','$rootScope',function($scope,$http,Auth,ngDialog,$window,$location,$rootScope){

	$scope.user = {};

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$window.location.href="my-account";
			}

		});

	}

	$scope.dialog_open = function(message, buttonShow, closeCallBack){

		$rootScope.message = message;
		$rootScope.buttonShow = buttonShow;
		$scope.dialog = ngDialog.open({
			template: 'registrationDialog',
			className: 'ngdialog-theme-plain',
			showClose: false
		});

		if(closeCallBack){
			$scope.dialog.closePromise.then(closeCallBack);
		}

	}

	$scope.dialog_close = function(){

		$scope.dialog.close();

	}

	$scope.resetUserPassword = function(){
        
        var source = $location.absUrl(),arr = source.split('/'),id=arr[arr.length - 1];
		$scope.user.token = id; 
		console.log($scope.user);
		$scope.dialog_open('Please wait...');

		$http.post('api/bounty/reset_password',$scope.user).success(function(data){

			$scope.dialog_close(); 
 			if(data.status=='success'){

 				$scope.user = {};
 				$scope.error_message = '';

 				callback = function(){
 					$window.location.href = 'login';
 				};
 				$scope.dialog_open(data.message, true, callback);

 			}else{

 				$scope.error_message = data.message;

 			}
				
 		});

 		return false;

	}

	$scope.auth();

}]);