angular.module('pledgeApp',['ngDialog','angularFileUpload'])
.controller('pledgeCtrl',['$scope','$http','$window','ngDialog','$upload', '$rootScope',function($scope,$http,$window,ngDialog,$upload,$rootScope){

	$scope.progress = -1;
	$scope.selectedFiles = [];
	$scope.months = [{val:1,name:'Jan'},
					 {val:2,name:'Feb'},
					 {val:3,name:'Mar'},
					 {val:4,name:'Apr'},
					 {val:5,name:'May'},
					 {val:6,name:'Jun'},
					 {val:7,name:'Jul'},
					 {val:8,name:'Aug'},
					 {val:9,name:'Sep'},
					 {val:10,name:'Oct'},
					 {val:11,name:'Nov'},
					 {val:12,name:'Dec'}];
	$scope.years = [];
	$scope.imageURL = false;
	$scope.user = {};

	$scope.dialog_open = function(message, buttonShow, closeCallBack){
		$rootScope.message = message;
		$rootScope.buttonShow = buttonShow;

		$scope.dialog = ngDialog.open({
			template: 'registrationDialog',
			className: 'ngdialog-theme-plain',
			showClose: true
		});

		if(closeCallBack){
			$scope.dialog.closePromise.then(closeCallBack);
		}

	}

	$scope.dialog_close = function(){

		$scope.dialog.close();

	}

	$scope.registerDialog = function(){

		ngDialog.open({
		    template: 'registrationForm',
		    className: 'ngdialog-theme-plain popForm',
		    controller: 'pledgeCtrl',
		    showClose: true,
		    overlay :false,
		    closeByDocument  :false
		});
	}


	$scope.onFileSelect = function($files){

		var i = 0;
		var $file = $files[i];

		$scope.selectedFiles = $files;
		// $scope.progress = 0;

		if (window.FileReader && $file.type.indexOf('image') > -1) {
			var fileReader = new FileReader();
			fileReader.readAsDataURL($files[i]);
			var loadFile = function(fileReader, index) {
				fileReader.onload = function(e) {
					$timeout(function() {
						$scope.imageURL = e.target.result;
					});
				}
			}(fileReader, i);
		}


		$upload.upload({
			url : 'api/bounty/upload_profile',
			method: 'POST',
			headers: {'my-header': 'my-header-value'},
			file: $scope.selectedFiles[i],
			fileFormDataName: 'image'
		}).then(function(response) {

			var response = response.data;
			if(!response.error){
				$scope.user.image = response.new_image;
				$scope.error = false;
			}else{
				$scope.error = response.error;
				$scope.dialog_open(response.error);
			}

		}, null, function(evt) {
			$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
		}).xhr(function(xhr){
			xhr.upload.addEventListener('abort', function(){console.log('aborted complete')}, false);
		});

	};


	$scope.birthYears = function() {

		var dateTime = new Date();
		var start = dateTime.getFullYear() - 90;
		var end = dateTime.getFullYear() - 18;
		var years = [];
		var i = 0;

		while(start<=end){
			years[i] = start;
			start++;
			i++;
		}

		$scope.years = years;

	}

	$scope.birthDays = function () {
	  var num = 31;
	  var i;
	  var days = [];
	  for (i = 1; i <= num; i++) {days.push(String(i)); }
	  return days;
	};


	$scope.register = function(isValid)
	{

		if(isValid){

 			$scope.dialog_open('Please wait...');

			$http.post('api/bounty/register',$scope.user).success(function(response){

				var callback = null;

				if(response.error!=0) {
					$scope.dialog_open(response.message, true, callback);

				} else {
					callback = function() {
						$window.location.href = 'registerSuccess';
					};
					callback();
				}
			});
		}
	}

	$scope.getMunicipal = function(){

		$http.get('api/bounty/cities?province='+$scope.user.province_id).success(function(data){
        	 $scope.cities = data.cities;
        	 $scope.user.city_id = $scope.cities[0];
        	 $scope.is_cities = true;
        });
	}

	$scope.getProvinces = function(){

    	$http.get('api/bounty/provinces').success(function(data){
        	 $scope.provinces = data.provinces;
        	 $scope.is_cities = false;
        });

     }

	$scope.birthYears();
	$scope.getProvinces();

}])
.directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           if (inputValue == undefined) return ''
           var transformedInput = inputValue.replace(/[^0-9+.]/g, '');
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }
           return transformedInput;
       });
     }
   };
});
