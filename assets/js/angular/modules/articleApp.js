'use strict';

angular.module('articleApp',['imageSlider','googleSearch','topPicks','socialShare','ngSanitize','ngDialog','Authentication'])
.controller('articleCtrl',['$scope','$http','socialShare','$location','ngDialog','Auth','$window',function($scope,$http,socialShare,$location,ngDialog,Auth,$window){

	$scope.article = {};
	$scope.article_comments = {};
	$scope.prev_article = null;
	$scope.next_article = null;
	$scope.parent_id = 0;
	$scope.user = {};
	$scope.replies = {};

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$scope.logged = 1;
			}

		});
	}

	$scope.logout = function(){
		Auth.logout().success(function(data){
			$window.location.href = "login";
		});
	}

	$scope.shareFB = function(id,page){
 		socialShare.FBSharePage(id,page,FB);
	};
	$scope.shareTwitter = function(id,page){
 		socialShare.TwitterSharePage(id,page);
	};
	$scope.sharePintirest = function(id,page){
 		socialShare.PinterestSharePage(id,page);
	};

	$scope.dialog_open = function(message,showClose,closeByDocument,closeByEscape){

		$scope.dialog = ngDialog.open({
								template: '<center>'+message+'</center>',
					 			plain: true,
					 			showClose:showClose,
					 			closeByDocument: closeByDocument,
								closeByEscape: closeByEscape
							});
	}

	$scope.dialog_close = function(){
		$scope.dialog.close();
	}


	$scope.loadArticle = function(){

		var source = $location.absUrl(),arr = source.split('/'),id=arr[arr.length - 2];

		$scope.page_title = arr[arr.length-1];

		$scope.dialog_open('Please wait...');

		$http.get('api/bounty/article?id='+id).success(function(data){
			$scope.article = data.article;
			$scope.prev_article = data.previous_article;
			$scope.next_article = data.next_article;
			$scope.loadComments();
			$scope.loadRelatedArticles();
			$scope.dialog_close();
 		});

	};

	$scope.addFavourite = function(){

		var id = $scope.article.id;

		$scope.dialog_open('Please wait...');

  		$http.post('api/bounty/favourite_article',{id:id}).success(function(response){	

 			 $scope.dialog_close();
 			 $scope.dialog_open(response.message,true);

		});

	};

	$scope.addComment = function(parent_id){

		var param = {id:$scope.article.id,parent_id:parent_id,comment:$scope.article.comment,comment_reply:$scope.article.comment_reply};

			if(!$scope.article.comment && !$scope.article.comment_reply){
				$scope.dialog_open('Write your comment to continue.',true,true,true);
				return false;
			}
			$scope.dialog_open('Please wait..');
 
			$http.post('api/bounty/article_comments',param).success(function(response){

	 			$scope.article.comment = '';
	 			$scope.article.comment_reply = '';
	 			$scope.dialog_close();
	 			$scope.dialog_open(response.message,true,true,true);
 
 			});

  		
	}

	$scope.replies_show = function(parent_id){

		var new_scope = 'subject'+parent_id;
		$scope.show_reply = [];

		$scope[new_scope] = true;
		$scope.show_reply[parent_id] = true;

		$http.get('api/bounty/article_comment_replies?id='+parent_id).success(function(data){

 			$scope.replies[parent_id] = data;

		});


	}

	$scope.loadComments = function(){

		$http.get('api/bounty/article_comments?id='+$scope.article.id).success(function(response){	

			$scope.article_comments = response;
			

		});

	}

	$scope.loadRelatedArticles = function(){

		$http.get('api/bounty/related_articles?page='+$scope.article.page).success(function(data){	

			$scope.related_articles = data;

		});

	}

	$scope.userDetails = function()
	{

	 	$http.get('api/bounty/user').success(function(data){
			$scope.user = data;
 		});

	}

	$scope.auth();
	$scope.loadArticle();
	$scope.userDetails();

}])

.filter('dateToISO', function() {
  return function(input) {
    return new Date(input).toISOString();
  };
});