angular.module('loginApp',['googleSearch','Authentication'])
.controller('loginCtrl',['$scope','$window','$http','Auth','$window',function($scope,$window,$http,Auth,$window){
	$scope.user = {};

	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
 				console.log(data);
 				// $window.location.href = "my-account";
			}

		});
	}

	$scope.login = function(isValid)
	{
		if(isValid){

			var inputs = $scope.user;
 
			Auth.login(inputs).success(function(data){

				if(data.error){
					$scope.login_message = data.message;
					$scope.user = {};
				} else {
					console.log(data);
					if( data.first_visit == 1 ) {
						$window.location.href = 'my-account';
					} else if( data.first_visit == 0 ) {
						$window.location.href = 'index';
					}
				}

			});

		}	

	};

	$scope.auth();


}]);