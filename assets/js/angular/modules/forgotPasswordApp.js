angular.module('forgotPasswordApp',['Authentication','ngDialog'])
.controller('forgotPasswordCtrl',['$scope','$http','Auth','ngDialog','$window','$rootScope',function($scope,$http,Auth,ngDialog,$window,$rootScope){
	$scope.user = {};
	$scope.auth = function(){

		Auth.load().success(function(data){
 			if(!data.error){
				$window.location.href="my-account";
			}

		});

	}

	$scope.dialog_open = function(message, buttonShow, closeCallBack){

		$rootScope.message = message;
		$rootScope.buttonShow = buttonShow;
		$scope.dialog = ngDialog.open({
			template: 'registrationDialog',
			className: 'ngdialog-theme-plain',
			showClose: false
		});

		if(closeCallBack){
			$scope.dialog.closePromise.then(closeCallBack);
		}

	}

	$scope.dialog_close = function(){

		$scope.dialog.close();

	}

	
	$scope.requestNewPassword = function(){

 		$http.post('api/bounty/request_new_password',$scope.user).success(function(data){
  
 			if(data.status){

 				$scope.user = {};
 				$scope.error_message = '';
 				$scope.message = data.message;
 				$scope.success = 1;
 				

 			}else{

 				$scope.success = 0;
 				$scope.error_message = data.message;

 			}
				
 		});
	}

	$scope.auth();


}]);