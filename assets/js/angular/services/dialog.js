'use strict';

angular.module('Dialog',['ngDialog'])

.factory('Dialog',['ngDialog',function($scope,ngDialog){

		return {

 			open: function(message){

			 ngDialog.open({
									template: '<center>'+message+'</center>',
									closeByDocument: false,
									closeByEscape: false,
									showClose:false
								});

			},
			close: function(){

			 ngDialog.close();

			}


 		};


}]);
