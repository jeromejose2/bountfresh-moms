'use strict';

angular.module('socialShare',[])

.factory('socialShare',['$window','$http','customWindow',function($window,$http,customWindow){
	
	return{

		FBSharePage: function(id,page,FB){
console.log('facebook');
			var base_url = $window.location.origin+''+$window.location.pathname,
			 base_url_orig = $window.location.origin+'/',
//				url = base_url+'/'+page+'/',
url = base_url,				
				obj = {};

  	 
	        $http.get('api/bounty/'+page+'?id='+id).success(function(data){
             //longUrl = url+data.id+'/'+data.url_title,
	        	var data = page=='recipe' ? data.recipe : data.article,
	       			longUrl = url,
	       			media_photo = base_url_orig+data.image,
	       			bitlyURL = '';
	       			console.log(data.description.length);
	if(data.description.length >= '10000')
	{
		console.log('limit the link');
		data.description = data.description.substring(1, 1000);
	}
	console.log(data.description.length);
	console.log(media_photo);
        console.log(data.url_title);
//	var desc = $(data.description).text();
//console.log(desc);

	            $http.get('http://api.bitly.com/v3/shorten?format=json&apiKey=R_212fef06aafb508c95bd620716a17860&login=o_1eb4rp8f6k&longUrl='+longUrl)
            	.success(function(response){
console.log('test-sccess');
console.log(longUrl);
console.log(longUrl.length);
console.log(data.title);
 	            	bitlyURL = response.data.url; 
	             	
					obj = {
						method: 'feed',
						link: longUrl,
						picture: media_photo,
						name: data.title,
						caption: '',
						description: data.description//desc
					};      

					
					FB.ui(obj,function(response){
						console.log(response);
					});				

	            });

	     	});

	    },

	    PinterestSharePage: function(id,page){

 console.log('pinterest');
			var base_url = $window.location.origin+''+$window.location.pathname,
			 base_url_orig = $window.location.origin+'/',
				url = base_url+'/'+page,
				title = 'Share';
	 console.log('api/bounty/'+page+'?id='+id);
			$http.get('api/bounty/'+page+'?id='+id).success(function(data){
				
				var data = page=='recipe' ? data.recipe : data.article,
					longUrl = url+'/'+data.id+'/'+data.url_title,
					media_photo = base_url_orig+data.image,
					bitlyURL = '';

				$http.get('http://api.bitly.com/v3/shorten?format=json&apiKey=R_212fef06aafb508c95bd620716a17860&login=o_1eb4rp8f6k&longUrl='+longUrl)
				.success(function(response){

					bitlyURL = response.data.url;
					console.log(data.description);
					url = 'http://www.pinterest.com/pin/create/button/?url='+bitlyURL+'&media='+media_photo+'&description='+data.description;
					customWindow.open(url,title);

				});
					
			});

		},
		TwitterSharePage: function(id,page){
 console.log('twitter');
			var url = 'http://twitter.com?status=',
				title = 'Share',
				base_url= $window.location.origin+''+$window.location.pathname,
				 base_url_orig = $window.location.origin+'/';

 
	        $http.get('api/bounty/'+page+'?id='+id).success(function(data){

	        	var data = page=='recipe' ? data.recipe : data.article,
	        		longUrl = encodeURIComponent(base_url_orig+'/'+page+'/'+data.id+'/'+data.url_title);

	        	 
	        	$http.get('http://api.bitly.com/v3/shorten?format=json&apiKey=R_212fef06aafb508c95bd620716a17860&login=o_1eb4rp8f6k&longUrl='+longUrl)
	        		.success(function(response){

 	        			var bitlyURL = response.data.url;
 	        			url += encodeURIComponent(data.title+' '+bitlyURL);
			        	customWindow.open(url,title);

	        		});	        	   
	        });

		}

	};
	
}])
.factory('customWindow',['$window',function($window){

	return{

		open: function(url,title){

			var w = 400,
			h = 300,
			left = (screen.width/2)-(w/2),
			top = (screen.height/2)-(h/2);

			$window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);

		}

	};
	
}]);
