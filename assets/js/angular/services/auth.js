angular.module('Authentication',[])
.factory('Auth',['$http',function($http){
	
	return {
		load : function(){
			return $http.get('api/bounty/auth');
		},
		login : function(inputs){
			return $http.post('api/bounty/login',inputs);
		},
		logout : function(){
			return $http.get('api/bounty/logout');
		}
	}

}]);