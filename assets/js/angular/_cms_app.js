'use strict';

var CMSApp = angular.module('CMSApp',['ngRoute','ngResource','CMSAppControllers','googlechart']);

CMSApp.config(['$routeProvider','$resourceProvider',function($routeProvider,$resourceProvider){

	


	$routeProvider
	.when('/login',{
		templateUrl:'login.html',
		controller:'loginCtrl',
		title:'Login'
	})
	.when('/home',{
		templateUrl:'home.html',
		controller:'homeCtrl',
		title:'Home'
	})
	.when('/kitchen-recipes/page/:page',{
		templateUrl:'recipe-list.html',
		controller:'recipesListCtrl',
		title:'The Kitchen'
 	})
 	.when('/kitchen-recipes/new/page/:page',{
		templateUrl:'recipe-add.html',
		controller:'recipesAddCtrl',
		title:'The Kitchen'
 	})
 	.when('/kitchen-recipes/id/:id/page/:page',{
		templateUrl:'recipe-edit.html',
		controller:'recipesEditCtrl',
		title:'The Kitchen'
 	})
 	/// Kitchen Articles
 	.when('/kitchen-articles/page/:page',{
		templateUrl:'article-list.html',
		controller:'articlesListCtrl',
		title:'The Kitchen'
 	})
 	.when('/kitchen-articles/new/page/:page',{
		templateUrl:'article-add.html',
		controller:'articlesAddCtrl',
		title:'The Kitchen'
 	})
 	.when('/kitchen-articles/id/:id/page/:page',{
		templateUrl:'article-edit.html',
		controller:'articlesEditCtrl',
		title:'The Kitchen'
 	})

 	// living room
 	.when('/living-room/page/:page',{
		templateUrl:'article-list.html',
		controller:'articlesListCtrl',
		title:'The Living Room'
 	})
 	.when('/living-room/new/page/:page',{
		templateUrl:'article-add.html',
		controller:'articlesAddCtrl',
		title:'The Living Room'
 	})
 	.when('/living-room/id/:id/page/:page',{
		templateUrl:'article-edit.html',
		controller:'articlesEditCtrl',
		title:'The Living Room'
 	})
 	
 	// Kid's Bedroom
 	.when('/kids-bedroom/page/:page',{
		templateUrl:'article-list.html',
		controller:'articlesListCtrl',
		title:'The Kids Bedroom'
 	})
 	.when('/kids-bedroom/new/page/:page',{
		templateUrl:'article-add.html',
		controller:'articlesAddCtrl',
		title:'The Kids Bedroom'
 	})
 	.when('/kids-bedroom/id/:id/page/:page',{
		templateUrl:'article-edit.html',
		controller:'articlesEditCtrl',
		title:'The Kids Bedroom'
 	})

 	// Masters's Bedroom
 	.when('/masters-bedroom/page/:page',{
		templateUrl:'article-list.html',
		controller:'articlesListCtrl',
		title:'The Masters Bedroom'
 	})
 	.when('/masters-bedroom/new/page/:page',{
		templateUrl:'article-add.html',
		controller:'articlesAddCtrl',
		title:'The Masters Bedroom'
 	})
 	.when('/masters-bedroom/id/:id/page/:page',{
		templateUrl:'article-edit.html',
		controller:'articlesEditCtrl',
		title:'The Masters Bedroom'
 	})

 	/// Registrants
 	.when('/registrants',{
		templateUrl:'registrants-list.html',
		controller:'registrantsListCtrl',
		title:'Registrants'
 	})

 	/// Privacy
 	.when('/privacy-policy',{
		templateUrl:'privacy-policy.html',
		controller:'privacyPolicyListCtrl',
		title:'Privacy Policy'
 	})

 	/// Terms and Conditions
 	.when('/terms-and-conditions',{
		templateUrl:'terms-and-conditions.html',
		controller:'termsAndConditionsListCtrl',
		title:'Terms and Conditions'
 	})

 	/// Bounty Fresh Foods Details
 	.when('/contact-details-bff',{
		templateUrl:'contact-details-bff.html',
		controller:'bountyFreshFoodsListCtrl',
		title:'Bounty Fresh Foods'
 	})

 	/// Bounty Agro Ventures Details
 	.when('/contact-details-bav',{
		templateUrl:'contact-details-bav.html',
		controller:'bountyAgroVenturesListCtrl',
		title:'Bounty Agro Ventures'
 	})

 	/// FAQ
 	.when('/faq',{
		templateUrl:'faq.html',
		controller:'faqEditCtrl',
		title:'FAQ'
 	})

 	/// Recipe Comments
 	.when('/recipe-comments',{
		templateUrl:'recipe-comment-list.html',
		controller:'recipeCommentListCtrl',
		title:'Recipe Comments'
 	})

 	.when('/recipe-comments-replies',{
		templateUrl:'recipe-comment-replies-list.html',
		controller:'recipeCommentRepliesListCtrl',
		title:'Recipe Comment Replies'
 	})

 	/// Article Comments
 	.when('/article-comments',{
		templateUrl:'article-comment-list.html',
		controller:'articleCommentListCtrl',
		title:'Article Comments'
 	})

 	.when('/article-comments-replies',{
		templateUrl:'article-comment-replies-list.html',
		controller:'articleCommentRepliesListCtrl',
		title:'Article Comment Replies'
 	})


 	// Image Headers

 	.when('/image_headers',{
		templateUrl:'image-header-list.html',
		controller:'image_headersListCtrl',
		title:'Image Headers'
 	})
 	.when('/image_headers/new',{
		templateUrl:'image-header-add.html',
		controller:'image_headersAddCtrl',
		title:'Image Headers'
 	})

	.otherwise({
		redirectTo:'/login'
	});
	

}]);