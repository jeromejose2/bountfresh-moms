angular.module('serviceControllers', [])
 
	.factory('Recipe', function($resource) {
	 return $resource('../api/cms/recipes',{id:'@_id',page:'@_page'},{
	 	update: {
	      method: 'PUT'
	    },
	    delete: {
	    	method:'DELETE',
	    	headers:{'Content-Type': 'application/json'},
	    	params: {id: '@_id'} 
	    }
	 }); // Note the full endpoint address
	})

	.factory('Article', function($resource) {
	 return $resource('../api/cms/articles',{id:'@_id',page:'@_page'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Registrant', function($resource) {
	 return $resource('../api/cms/registrants',{id:'@_id',page:'@_page'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Graph', function($resource) {
	 return $resource('../api/cms/home',{},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Summary_Registrants', function($resource) {
	 return $resource('../api/cms/summary_registrants',{},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Summary_Comments', function($resource) {
	 return $resource('../api/cms/summary_comments',{},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Summary_Registrants_Month', function($resource) {
	 return $resource('../api/cms/summary_registrants_per_month',{},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Summary_Comments_Month', function($resource) {
	 return $resource('../api/cms/summary_comments_per_month',{},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Privacy_policy', function($resource) {
	 return $resource('../api/cms/privacy_policy',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Terms_and_conditions', function($resource) {
	 return $resource('../api/cms/terms_and_conditions',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('FAQ', function($resource) {
	 return $resource('../api/cms/faq',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Recipe_Comment', function($resource) {
	 return $resource('../api/cms/recipe_comments',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Recipe_Comment_Replies', function($resource) {
	 return $resource('../api/cms/recipe_comments_replies',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Article_Comment', function($resource) {
	 return $resource('../api/cms/article_comments',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Article_Comment_Replies', function($resource) {
	 return $resource('../api/cms/article_comments_replies',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Image_header', function($resource) {
	 return $resource('../api/cms/image_headers',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Bounty_fresh_foods', function($resource) {
	 return $resource('../api/cms/bounty_fresh_foods',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Bounty_agro_ventures', function($resource) {
	 return $resource('../api/cms/bounty_agro_ventures',{id:'@_id'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})

	.factory('Pledge', function($resource) {
	 return $resource('../api/cms/pledges',{id:'@_id',page:'@_page'},{
	 	update: {
	      method: 'PUT'
	    }
	 }); // Note the full endpoint address
	})
	
	.factory('Accounts', function($resource) {
		return $resource('../api/cms/accounts', {id:'@_id'}, {
			update: {
				method: 'PUT'
			},
			delete: {
				method: 'DELETE',
				headers: {'Content-Type':'application/json'},
				params: {id:'@_id'}
			}
		});
	});