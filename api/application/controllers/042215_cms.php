<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require APPPATH.'/libraries/REST_Controller.php';


class Cms extends REST_Controller {


    public function login_check_get()
    {
        // Dummy
    }

    public function login_post(){

        $where = array('username'=>$this->post('username'),'password'=>md5($this->post('password')));
        $login = $this->global_model->get_row(array('table'=>'cms_user','where'=>$where));
         
        if($login){
            $message = array('logged_in'=>true,'login_message'=>'Successfully logged in.'); 
            $this->session->set_userdata(array('user_id'=>$login->user_id,'name'=>$login->name,'admin_logged_in'=>true));           
        }else{
            $message = array('logged_in'=>false,'login_message'=>'Invalid username/password. Please try again.');
        }

        $this->response($message, 200);
                           
    }
    
    public function logout_get(){

        $array_items = array('user_id'=>'','name'=>'','admin_logged_in'=>'');
        $this->session->unset_userdata($array_items);
        $this->response(array('login_message'=>'Successfully log out.'), 200); 

    }

    private function get_days_home($param){
        $date = date("t", strtotime($param));
            for($i = 1; $i <= $date; $i++) {
                $every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);
                $data[] = array($every_date);
            }
            return $data;
    }

    function home_get(){

        $raw_result = $this->global_model->get_rows(array('table'=>'tbl_registrants'))->result_array();

        foreach($raw_result as $k => $v) {
            $final[$k]['date_created'] = $v['date_added'];
        }

        $date1 = @date('Y-m-d', strtotime(min($final[0])));
        $date2 = @date('Y-m-d', strtotime(max($final[0])));

        $dates_count = $this->get_days_home($date1);

        for ($i = 0; $i < count($dates_count); $i++) { 
            $param = array(
                'table'=>'tbl_registrants',
                'where'=>"date(date_added) = '" . $dates_count[$i][0] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items = $this->global_model->get_rows($param)->result_array();
            $param = array(
                'table'=>'tbl_article_comments',
                'where'=>"date(date_added) = '" . $dates_count[$i][0] . "' AND approval_status = 1",
                'fields' => 'COUNT(*) as total'
            );
            $items_comments_article = $this->global_model->get_rows($param)->result_array();
            $param = array(
                'table'=>'tbl_recipe_comments',
                'where'=>"date(date_added) = '" . $dates_count[$i][0] . "' AND approval_status = 1",
                'fields' => 'COUNT(*) as total'
            );
            $items_comments_recipes = $this->global_model->get_rows($param)->result_array();
            $daily_registrants['daily'][] = array($dates_count[$i][0], $items[0]['total'], $items_comments_article[0]['total'] + $items_comments_recipes[0]['total']);
        }

        $rows = $daily_registrants;

        $column_data = array();
        $inc = 0;

        foreach($rows['daily'] as $k => $v) {
            $column_data[$k]["c"] = array(
                                            array(
                                                    "v" => $v[0]
                                                ),
                                            array(
                                                    "v" => $v[1],
                                                    "f" => NULL
                                                ),
                                            array(
                                                    "v" => $v[2],
                                                    "f" => NULL
                                                )
                                        );
        }

        $arr = array(
              "type" => "AreaChart",
              "cssStyle" => "width: 100%",
              "data" => array(
                          "cols" => array(
                                      array(
                                            "id" => "registrants",
                                            "label" => "Registrants",
                                            "type" => "string",
                                            "p" => array()
                                          ),
                                      array(
                                            "id" => "total registrants",
                                            "label" => "Registrants",
                                            "type" => "number",
                                            "p" => array()
                                          ),
                                      array(
                                            "id" => "total comments",
                                            "label" => "Comments",
                                            "type" => "number",
                                            "p" => array()
                                          )
                                    ),
                          "rows" => $column_data
                        ),
              "options" => array(
                                  "title" => "Registrants & Comments per day",
                                  "isStacked" => "true",
                                  "fill" => 20,
                                  "displayExactValues" => TRUE,
                                  "vAxis" => array(
                                                    "title" => "",
                                                    "minValue" => 0,
                                                    "format" => "#"
                                                  ),
                                  "hAxis" => array(
                                                    "title" => ""
                                                  )
                                ),
              "formatters" => array(
                                    ),
              "displayed" => TRUE
          );

        $chart = $arr;

        $this->response($chart, 200);

    }

    private function summary_get_days($param){
        $date = date("t", strtotime($param));
        for($i = 1; $i <= $date; $i++) {
            $every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);
            $data[] = array($every_date);
        }
        return $data;
    }

    function summary_registrants_get(){

        $raw_result = $this->global_model->get_rows(array('table'=>'tbl_registrants'))->result_array();

        foreach($raw_result as $k => $v) {
            $final[$k]['date_created'] = $v['date_added'];
        }

        $date1 = @date('Y-m-d', strtotime(min($final[0])));
        $date2 = @date('Y-m-d', strtotime(max($final[0])));

        $dates_count = $this->summary_get_days($date1);

        for ($i = 0; $i < count($dates_count); $i++) { 
            $param = array(
                'table'=>'tbl_registrants',
                'where'=>"date(date_added) = '" . $dates_count[$i][0] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items = $this->global_model->get_rows($param)->result_array();
            $daily_registrants['daily'][] = array($dates_count[$i][0], $items[0]['total']);
        }

        $rows = $daily_registrants;

        $this->response($rows, 200);

    }

    private function summary_comments_days($param){
        $date = date("t", strtotime($param));
        for($i = 1; $i <= $date; $i++) {
            $every_date = date("Y") . "-" . date("m") . "-" . str_pad($i, 2, "0", STR_PAD_LEFT);
            $data[] = array($every_date);
        }
        return $data;
    }

    function summary_comments_get(){

        $raw_result = $this->global_model->get_rows(array('table'=>'tbl_article_comments'))->result_array();

        foreach($raw_result as $k => $v) {
            $final[$k]['date_created'] = $v['date_added'];
        }

        $date1 = @date('Y-m-d', strtotime(min($final[0])));
        $date2 = @date('Y-m-d', strtotime(max($final[0])));

        $dates_count = $this->summary_comments_days($date1);

        for ($i = 0; $i < count($dates_count); $i++) { 
            $param = array(
                'table'=>'tbl_article_comments',
                'where'=>"date(date_added) = '" . $dates_count[$i][0] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items = $this->global_model->get_rows($param)->result_array();
            $param = array(
                'table'=>'tbl_recipe_comments',
                'where'=>"date(date_added) = '" . $dates_count[$i][0] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items2 = $this->global_model->get_rows($param)->result_array();
            $daily_registrants['daily'][] = array($dates_count[$i][0], $items[0]['total'] + $items2[0]['total']);
        }

        $rows = $daily_registrants;

        $this->response($rows, 200);

    }

    private function summary_registrants_per_month($date1, $date2){
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date("mY", $time2);

        while ($time1 < $time2) {
            $time1 = strtotime(date("Y-m-d", $time1) . " +1 month");
            if(date("mY", $time1) != $my && ($time1 < $time2)) {
                $months[] = array(date("Y-m-01", $time1), date("Y-m-t", $time1), date("F"));
            }
        }

        return $months;
    }

    function summary_registrants_per_month_get() {

        $param = array(
            'table' => 'tbl_registrants'
        );
        $r = $this->global_model->get_rows($param)->result_array();

        $months_arr = array();

        foreach($r as $k => $v) {
            $months_arr[] = date('Y-m-d', strtotime($v['date_added']));
        }

        $start_month = "2014-10-01";
        $end_month = max($months_arr);

        $months_count = $this->summary_registrants_per_month($start_month, $end_month);

        for($i = 0; $i < count($months_count); $i++) {
            $param = array(
                'table' => 'tbl_registrants',
                'where' => "date(date_added) between '" . $months_count[$i][0] . "' AND '" . $months_count[$i][1] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items = $this->global_model->get_rows($param)->result_array();
            if($items) $registrants_monthly['monthly'][] = array($months_count[$i][0], $items[0]['total'], $months_count[$i][1], $months_count[$i][2]);
            else $registrants_monthly['monthly'][] = array($months_count[$i][0], 0, $months_count[$i][1], $months_count[$i][2]);
        }

        $rows = $registrants_monthly;

        $this->response($rows, 200);

    }

    private function summary_comments_per_month($date1, $date2){
            $time1 = strtotime($date1);
            $time2 = strtotime($date2);
            $my = date("mY", $time2);

            while ($time1 < $time2) {
                $time1 = strtotime(date("Y-m-d", $time1) . " +1 month");
                if(date("mY", $time1) != $my && ($time1 < $time2)) {
                    $months[] = array(date("Y-m-01", $time1), date("Y-m-t", $time1), date("F"));
                }
            }

            return $months;
    }

    function summary_comments_per_month_get() {

        $param = array(
            'table' => 'tbl_article_comments'
        );
        $ac = $this->global_model->get_rows($param)->result_array();

        $param = array(
            'table' => 'tbl_recipe_comments'
        );
        $rc = $this->global_model->get_rows($param)->result_array();

        $combined_comments = array_merge($ac, $rc);

        $months_arr = array();

        foreach($combined_comments as $k => $v) {
            $months_arr[] = date('Y-m-d', strtotime($v['date_added']));
        }

        $start_month = "2014-10-01";
        $end_month = max($months_arr);

        $months_count = $this->summary_comments_per_month($start_month, $end_month);

        for($i = 0; $i < count($months_count); $i++) {
            $param = array(
                'table' => 'tbl_recipe_comments',
                'where' => "date(date_added) between '" . $months_count[$i][0] . "' AND '" . $months_count[$i][1] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items = $this->global_model->get_rows($param)->result_array();
            $param = array(
                'table' => 'tbl_article_comments',
                'where' => "date(date_added) between '" . $months_count[$i][0] . "' AND '" . $months_count[$i][1] . "'",
                'fields' => 'COUNT(*) as total'
            );
            $items2 = $this->global_model->get_rows($param)->result_array();
            if($items) $comments_monthly['monthly'][] = array($months_count[$i][0], $items[0]['total'] + $items2[0]['total'], $months_count[$i][1], $months_count[$i][2]);
            else $comments_monthly['monthly'][] = array($months_count[$i][0], 0, $months_count[$i][1], $months_count[$i][2]);
        }

        $rows = $comments_monthly;

        $this->response($rows, 200);

    }
	 
	function recipes_get(){

        $where = $this->get('id') ? array('recipe_id'=>$this->get('id'),'page'=>$this->get('page')) : array('page'=>$this->get('page'));
        $recipes = $this->global_model->get_rows(array('table'=>'recipes',
                                                         'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                        'where'=>$where)
                                                );
        $this->load->helper('text');
        
        if($recipes->num_rows())
        {
            
            $rows = array();
            foreach($recipes->result() as $v){
                $description = strip_tags($v->description);
                if(!$this->get('id'))
                    $rows[] = array('id'=>$v->recipe_id,
                                    'image'=>'../assets/img/recipes/resized/50_50_'.$v->image,
                                    'title'=>$v->title,'description'=>character_limiter($description,20),
                                    'top_picked'=>$v->top_picked ? 'Yes' : 'No',
                                    'featured'=>$v->featured ? 'Yes' : 'No',
                                    'page'=>$v->page,
                                    'privacy_status'=>$v->privacy_status,
                                    'date_added'=>date('F d,Y',strtotime($v->date_added)));
                else{

                    $rows = array('id'=>$v->recipe_id,
                                    'image'=>$v->image,
                                    'title'=>$v->title,
                                    'description'=>$v->description,
                                    'meal_budget'=>$v->meal_budget,
                                    'meal_type'=>$v->meal_type,
                                    'featured'=>$v->featured,
                                    'top_picked'=>$v->top_picked,
                                    'page'=>$v->page,
                                    'privacy_status'=>$v->privacy_status,
                                    'date_added'=>date('F d,Y',strtotime($v->date_added)));

                }
                     
            }
            $this->response($rows, 200); // 200 being the HTTP response code
        }

        else
        {
            $this->response(array(), 200);
        }

	}

    function recipes_post(){
        
        $this->load->helper('url');
        $data = $this->post();
              
        $data['url_title'] = url_title($data['title']);
        $data['date_added'] = $data['privacy_status'] ? array(array('field'=>'date_published','value'=>'NOW()'),array('field'=>'date_added','value'=>'NOW()')) : true;
        $id = $this->global_model->insert('recipes',$data);
        $message = array('id' => $id, 'name' => $this->post('title'),'message'=>'ADDED!');        
        $this->response($message);

    }


    function recipes_put(){

        $recipe = $this->global_model->get_row(array('table'=>'recipes','where'=>array('recipe_id'=>$this->put('id'))));
        
        $this->load->helper('url');
        $data = $this->put();
        if(isset($data['date_added']))
            unset($data['date_added']);
        $data['date_modified'] = true;                 
        $data['url_title'] = url_title($data['title']);
        if($recipe && !$recipe->privacy_status && $data['privacy_status'])
        {
            $data['date_modified'] = array(array('field'=>'date_published','value'=>'NOW()'),array('field'=>'date_modified','value'=>'NOW()'));
        }
        $this->global_model->update('recipes',$data,array('recipe_id'=>$this->put('id')));
        $message = array('id' => $this->put('id'), 'name' => $this->put('title'),'message'=>'UPDATED!');        
        $this->response($data); 
        
    } 

    function recipes_delete(){
         
        $recipe = $this->global_model->get_row(array('table'=>'recipes','where'=>array('recipe_id'=>$this->delete('id'))));

        if($recipe){

            $this->global_model->delete('recipes',array('recipe_id'=>$this->delete('id')));
            
            $file_path = '../assets/img/articles/';
            
            if(file_exists($file_path.$recipe->image))
                unlink($file_path.$recipe->image);
            
            if(file_exists($file_path.'50_50_'.$recipe->image))
                unlink($file_path.'50_50_'.$recipe->image);

            if(file_exists($file_path.'250_250'.$recipe->image))
                unlink($file_path.'250_250'.$recipe->image);

            $message = array('id' =>$this->delete('id'), 'message' => 'DELETED!');
        
        }else
            $message = array('id' =>$this->delete('id'), 'message' => 'Error to delete!'); 


        $this->response($message);
    }
 
 
    function articles_get(){

        $where = $this->get('id') ? array('article_id'=>$this->get('id'),'page'=>$this->get('page')) : array('page'=>$this->get('page'));
        $articles = $this->global_model->get_rows(array('table'=>'articles',
                                                         'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                        'where'=>$where)
                                                );
        $this->load->helper('text');
        
        if($articles->num_rows())
        {
            
            $rows = array();
            foreach($articles->result() as $v){
                $description = strip_tags($v->description);
                if(!$this->get('id'))
                    $rows[] = array('id'=>$v->article_id,
                                    'image'=>'../assets/img/articles/resized/50_50_'.$v->image,
                                    'title'=>$v->title,
                                    'description'=>character_limiter($description,20),
                                    'promo'=>($v->promo) ? 'Yes' : 'No',
                                    'author'=>$v->author,
                                    'featured'=>$v->featured ? 'Yes' : 'No',
                                    'top_picked'=>$v->top_picked ? 'Yes' : 'No',
                                    'page'=>$v->page,
                                    'privacy_status'=>$v->privacy_status,
                                    'date_added'=>date('F d,Y',strtotime($v->date_added)));
                else
                    $rows = array('id'=>$v->article_id,
                                    'image'=>$v->image,
                                    'title'=>$v->title,
                                    'description'=>$v->description,
                                    'promo'=>$v->promo,
                                    'author'=>$v->author,
                                    'featured'=>$v->featured,
                                    'top_picked'=>$v->top_picked,
                                    'page'=>$v->page,
                                    'privacy_status'=>$v->privacy_status,
                                    'date_added'=>date('F d,Y',strtotime($v->date_added))); 
            }
            $this->response($rows, 200); // 200 being the HTTP response code
        }

        else
        {
             $this->response(array(), 200);
        }

    }

    function articles_post(){
        
        $this->load->helper('url');
        $data = $this->post();
               
        $data['url_title'] = url_title($data['title']);
        $data['date_added'] = $data['privacy_status'] ? array(array('field'=>'date_published','value'=>'NOW()'),array('field'=>'date_added','value'=>'NOW()')) : true;
        $id = $this->global_model->insert('articles',$data);
        $message = array('id' => $id, 'name' => $this->post('title'),'message'=>'ADDED!');        
        $this->response($message,200);

    }


    function articles_put(){
        
        $article = $this->global_model->get_row(array('table'=>'articles','where'=>array('article_id'=>$this->put('id'))));

        $this->load->helper('url');
        $data = $this->put();
        if(isset($data['date_added']))
            unset($data['date_added']);
        $data['date_modified'] = true;                 
        $data['url_title'] = url_title($data['title']);

        if($article && !$article->privacy_status && $data['privacy_status'])
        {
            $data['date_modified'] = array(array('field'=>'date_published','value'=>'NOW()'),array('field'=>'date_modified','value'=>'NOW()'));
        }


        $this->global_model->update('articles',$data,array('article_id'=>$this->put('id')));
        $message = array('id' => $this->put('id'), 'name' => $this->put('title'),'message'=>'UPDATED!');        
        $this->response($data,200); 
        
    }

    function articles_delete(){
      
        $article = $this->global_model->get_row(array('table'=>'articles','where'=>array('article_id'=>$this->delete('id'))));

        if($article){

            $this->global_model->delete('articles',array('article_id'=>$this->delete('id')));
            
            $file_path = '../assets/img/articles/';
            
            if(file_exists($file_path.$article->image))
                unlink($file_path.$article->image);
            
            if(file_exists($file_path.'50_50_'.$article->image))
                unlink($file_path.'50_50_'.$article->image);

            if(file_exists($file_path.'250_250'.$article->image))
                unlink($file_path.'250_250'.$article->image);

            $message = array('id' =>$this->delete('id'), 'message' => 'DELETED!');
        
        }else
            $message = array('id' =>$this->delete('id'), 'message' => 'Error to delete!'); 


        $this->response($message);
    }

    function image_headers_get(){

        $where = $this->get('id') ? array('image_headers_id'=>$this->get('id')) : array();
        $image_headers = $this->global_model->get_rows(array('table'=>'image_headers',
                                                         'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                        'where'=>$where)
                                                );
        $this->load->helper('text');
        
        if($image_headers->num_rows())
        {
            
            $rows = array();
            $pages = array('1'=>'Home','2'=>'The Kitchen','3'=>'The Living Room','4'=>'The Kid\'s Bedroom','5'=>'The Master Bedroom');
            foreach($image_headers->result() as $v){

                if(!$this->get('id'))
                    $rows[] = array('id'=>$v->image_header_id,
                                    'image'=>'../assets/img/sliders/resized/200_200_'.$v->image,
                                    'page'=>$pages[$v->page],
                                    'date_added'=>date('F d,Y',strtotime($v->date_added)));
                else
                    $rows = array('id'=>$v->image_header_id,
                                    'image'=>$v->image,
                                    'page'=>$v->page,
                                    'date_added'=>date('F d,Y',strtotime($v->date_added))); 
            }

            $this->response($rows, 200);

        }

        else
        {
             $this->response(array(), 200);
        }

    }


    function image_headers_post(){
        
        $data = $this->post();
        $images = $data['images'];
         foreach($images as $i){
            $this->global_model->insert('image_headers',array('page'=>$data['page'],'image'=>$i,'date_added'=>true));
        }        
        $message = array('message'=>'ADDED!');        
        $this->response($message,200);

    }


    function image_headers_put(){
        
        $data = $this->put();

        if(isset($data['date_added']))
            unset($data['date_added']);

        $data['date_modified'] = true;                 
        $this->global_model->update('image_headers',$data,array('image_header_id'=>$this->put('id')));
        $message = array('id' => $this->put('id'),'message'=>'UPDATED!');        
        $this->response($message,200); 
        
    }

    function image_headers_delete(){
        

        $item = $this->global_model->get_row(array('table'=>'image_headers','where'=>array('image_header_id'=>$this->delete('id'))));

        if($item){

            $this->global_model->delete('image_headers',array('image_header_id'=>$this->delete('id')));
            
            $file_path = '../assets/img/sliders/';
            
            if(file_exists($file_path.$item->image))
                unlink($file_path.$item->image);
            
            if(file_exists($file_path.'50_50_'.$item->image))
                unlink($file_path.'50_50_'.$item->image);

            if(file_exists($file_path.'200_200_'.$item->image))
                unlink($file_path.'200_200_'.$item->image);

            $message = array('id' =>$this->delete('id'), 'message' => 'DELETED!');
        
        }else
            $message = array('id' =>$this->delete('id'), 'message' => 'Error to delete!'); 

         $this->response($message,200);
    }

    function pages_get(){
         $items = $this->global_model->get_row(array('table'=>'pages'));
         $this->response($items,200);
    }

    function comments_get(){

        $items = $this->global_model->get_row(array('table'=>'comments','order_by'=>array('field'=>'date_added','order'=>'DESC')));
        $this->response($items,200);

    }

    function registrants_get(){

        $rows = $this->global_model->get_rows(array('table'=>'registrants',
                                                    'fields'=>'tbl_registrants.*, tbl_provinces.province as province_name',
                                                    'join'=>array('table'=>'tbl_provinces','on'=>'tbl_provinces.province_id=tbl_registrants.province_id'),
                                                    'order_by'=>array('field'=>'tbl_registrants.date_added','order'=>'DESC')));

        // $rows = $this->global_model->get_rows(array('table'=>'registrants',
        //                                             'fields'=>'*, province as province_name',
        //                                             'order_by'=>array('field'=>'date_added','order'=>'DESC')));
        $rows = $this->db->select()
                    ->from('tbl_registrants')
                    ->get();   

        $items = array();

        if($rows->num_rows()){

            foreach($rows->result() as $v){
                $image = '../assets/img/registrants/resized/50_50_'.$v->image;
                $image = (file_exists($image) && $v->image) ? $image : '';
                $items[] = array('image'=> $image,
                                'name'=>$v->firstname.' '.$v->lastname,
                                'mobile'=>$v->mobile,
                                'birthdate'=>date('m/d/Y',strtotime($v->birthdate)),
                                'gender'=>$v->gender,
                                'email'=>$v->email,
                                // 'province'=>$v->province_name,
                                // 'city'=>$v->city,
                                'address'=>$v->address,
                                'status'=>$v->status,
                                'id'=>$v->registrant_id
                                );
            }

        }else{
            $items = array();
        }

        $this->response($items,200);

    }

    function registrants_delete(){

        $registrant = $this->global_model->get_row(array('table'=>'registrants','where'=>array('registrant_id'=>$this->delete('id'))));

        if($registrant){

            $this->global_model->delete('registrants',array('registrant_id'=>$this->delete('id')));
            
            $file_path = '../assets/img/registrants/';
            
            if(file_exists($file_path.$registrant->image))
                unlink($file_path.$registrant->image);

            if(file_exists($file_path.'50_50_'.$registrant->image))
                unlink($file_path.'50_50_'.$registrant->image);

            if(file_exists($file_path.'102_102_'.$registrant->image))
                unlink($file_path.'102_102_'.$registrant->image);

            $message = array('id' =>$this->delete('id'), 'message' => 'DELETED!');
        
        }else
            $message = array('id' =>$this->delete('id'), 'message' => 'Error to delete!'); 


        $this->response($message,200);

    }

    function privacy_policy_get(){

        $items = (array)$this->global_model->get_row(array('table'=>'privacy_policy','fields'=>'privacy_id as id, content','limit'=>1,'where'=>array('privacy_id'=>$this->get('id'))));
        $this->response($items,200);

    }

    function privacy_policy_put(){

        $data = $this->put();
        $data['date_modified'] = true;
        $this->global_model->update('privacy_policy',$data,array('privacy_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully updated'),200);        

    }

    function terms_and_conditions_get(){

        $items = (array)$this->global_model->get_row(array('table'=>'terms_and_conditions','fields'=>'terms_and_conditions_id as id, content','limit'=>1,'where'=>array('terms_and_conditions_id'=>$this->get('id'))));
        $this->response($items,200);

    }

    function terms_and_conditions_put(){

        $data = $this->put();
        $data['date_modified'] = true;
        $this->global_model->update('terms_and_conditions',$data,array('terms_and_conditions_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully updated'),200);        

    }

    function bounty_fresh_foods_get(){

        $items = (array)$this->global_model->get_row(array('table'=>'contact_details','fields'=>'contact_details_id as id, content','limit'=>1,'where'=>array('contact_details_id'=>$this->get('id'))));
        $this->response($items,200);

    }

    function bounty_fresh_foods_put(){

        $data = $this->put();
        $data['date_modified'] = true;
        $this->global_model->update('contact_details',$data,array('contact_details_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully updated'),200);        

    }

    function bounty_agro_ventures_get(){

        $items = (array)$this->global_model->get_row(array('table'=>'contact_details','fields'=>'contact_details_id as id, content','limit'=>1,'where'=>array('contact_details_id'=>$this->get('id'))));
        $this->response($items,200);

    }

    function bounty_agro_ventures_put(){

        $data = $this->put();
        $data['date_modified'] = true;
        $this->global_model->update('contact_details',$data,array('contact_details_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully updated'),200);        

    }

    function faq_get(){

        $items = (array)$this->global_model->get_row(array('table'=>'faq','fields'=>'faq_id as id, content','limit'=>1,'where'=>array('faq_id'=>$this->get('id'))));
        $this->response($items,200);

    }

    function faq_put(){

        $data = $this->put();
        $data['date_modified'] = true;
        $this->global_model->update('faq',$data,array('faq_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully updated'),200);        

    }


    function recipe_comments_get(){

        $rows = $this->global_model->get_rows(array('table'=>'recipe_comments',
                                                    'where'=>'parent_id = 0',
                                                    'fields'=>'tbl_recipe_comments.*, tbl_registrants.firstname, tbl_registrants.lastname,tbl_recipes.image as recipe_image,tbl_recipes.title as recipe_name',
                                                    'join'=>array(array('table'=>'tbl_registrants','on'=>'tbl_recipe_comments.registrant_id=tbl_registrants.registrant_id'),
                                                                  array('table'=>'tbl_recipes','on'=>'tbl_recipes.recipe_id=tbl_recipe_comments.recipe_id')
                                                                ),
                                                    'order_by'=>array('field'=>'tbl_recipe_comments.date_added','order'=>'DESC')
                                                    )
                                            );
        $items = array();
        if($rows->num_rows()){

            foreach($rows->result() as $v){
                
                if($v->approval_status==1)
                    $approval_status = 'Approved';
                else if($v->approval_status==2)
                    $approval_status = 'Disapproved';
                else
                    $approval_status = 'Pending';

                $items[] = array('registrant'=>$v->firstname.' '.$v->lastname,
                                'recipe_image'=>'../assets/img/recipes/resized/50_50_'.$v->recipe_image,
                                'comment'=>$v->comment,
                                'approval_status'=>$approval_status,
                                'status'=>$v->approval_status,
                                'date_added'=>date('F d, y',strtotime($v->date_added)),
                                'recipe_name'=>$v->recipe_name,
                                'id'=>$v->recipe_comment_id
                                );
            }

        }
        $this->response($items,200);


    }

    function recipe_comments_replies_get(){

        $rows = $this->global_model->get_rows(array('table'=>'recipe_comments',
                                                    'where'=>'parent_id != 0',
                                                    'fields'=>'tbl_recipe_comments.*, tbl_registrants.firstname, tbl_registrants.lastname,tbl_recipes.image as recipe_image,tbl_recipes.title as recipe_name',
                                                    'join'=>array(array('table'=>'tbl_registrants','on'=>'tbl_recipe_comments.registrant_id=tbl_registrants.registrant_id'),
                                                                  array('table'=>'tbl_recipes','on'=>'tbl_recipes.recipe_id=tbl_recipe_comments.recipe_id')
                                                                ),
                                                    'order_by'=>array('field'=>'tbl_recipe_comments.date_added','order'=>'DESC')
                                                    )
                                            );
        $items = array();
        if($rows->num_rows()){

            foreach($rows->result() as $v){
                
                if($v->approval_status==1)
                    $approval_status = 'Approved';
                else if($v->approval_status==2)
                    $approval_status = 'Disapproved';
                else
                    $approval_status = 'Pending';

                $param = array(
                    'table'=>'tbl_recipe_comments',
                    'where'=>'recipe_comment_id = ' . $v->parent_id,
                    'fields'=>'comment'
                );
                $comment_origin = $this->global_model->get_rows($param)->result_array();

                $items[] = array('registrant'=>$v->firstname.' '.$v->lastname,
                                'recipe_image'=>'../assets/img/recipes/resized/50_50_'.$v->recipe_image,
                                'comment'=>$v->comment,
                                'approval_status'=>$approval_status,
                                'status'=>$v->approval_status,
                                'date_added'=>date('F d, y',strtotime($v->date_added)),
                                'recipe_name'=>$v->recipe_name,
                                'id'=>$v->recipe_comment_id,
                                'comment_origin'=>$comment_origin[0]['comment']
                                );
            }

        }
        $this->response($items,200);


    }

    function recipe_comments_replies_put(){ 

        $status = $this->put('approval_status');
        $approval_status = array('0'=>'pending','1'=>'approved.','2'=>'disapproved.');
        $this->global_model->update('recipe_comments',array('approval_status'=>$status),array('recipe_comment_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully '.$approval_status[$status]),200);

    }

    function recipe_comments_put(){ 

        $status = $this->put('approval_status');
        $approval_status = array('0'=>'pending','1'=>'approved.','2'=>'disapproved.');
        $this->global_model->update('recipe_comments',array('approval_status'=>$status),array('recipe_comment_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully '.$approval_status[$status]),200);

    }


    function article_comments_get(){

        $rows = $this->global_model->get_rows(array('table'=>'article_comments',
                                                    'where'=>'parent_id = 0',
                                                    'fields'=>'tbl_article_comments.*, tbl_registrants.firstname, tbl_registrants.lastname,tbl_articles.image as article_image,tbl_articles.title as article_name',
                                                    'join'=>array(array('table'=>'tbl_registrants','on'=>'tbl_article_comments.registrant_id=tbl_registrants.registrant_id'),
                                                                  array('table'=>'tbl_articles','on'=>'tbl_articles.article_id=tbl_article_comments.article_id')
                                                                ),
                                                    'order_by'=>array('field'=>'tbl_article_comments.date_added','order'=>'DESC')
                                                    )
                                            );
        $items = array();
        if($rows->num_rows()){

            foreach($rows->result() as $v){
                
                if($v->approval_status==1)
                    $approval_status = 'Approved';
                else if($v->approval_status==2)
                    $approval_status = 'Disapproved';
                else
                    $approval_status = 'Pending';

                $items[] = array('registrant'=>$v->firstname.' '.$v->lastname,
                                'article_image'=>'../assets/img/articles/resized/50_50_'.$v->article_image,
                                'comment'=>$v->comment,
                                'approval_status'=>$approval_status,
                                'status'=>$v->approval_status,
                                'date_added'=>date('F d, y',strtotime($v->date_added)),
                                'article_name'=>$v->article_name,
                                'id'=>$v->article_comment_id
                                );
            }

        }
        $this->response($items,200);


    }

    function article_comments_replies_get(){

        $rows = $this->global_model->get_rows(array('table'=>'article_comments',
                                                    'where'=>'parent_id != 0',
                                                    'fields'=>'tbl_article_comments.*, tbl_registrants.firstname, tbl_registrants.lastname,tbl_articles.image as article_image,tbl_articles.title as article_name',
                                                    'join'=>array(array('table'=>'tbl_registrants','on'=>'tbl_article_comments.registrant_id=tbl_registrants.registrant_id'),
                                                                  array('table'=>'tbl_articles','on'=>'tbl_articles.article_id=tbl_article_comments.article_id')
                                                                ),
                                                    'order_by'=>array('field'=>'tbl_article_comments.date_added','order'=>'DESC')
                                                    )
                                            );
        $items = array();
        if($rows->num_rows()){

            foreach($rows->result() as $v){
                
                if($v->approval_status==1)
                    $approval_status = 'Approved';
                else if($v->approval_status==2)
                    $approval_status = 'Disapproved';
                else
                    $approval_status = 'Pending';

                $param = array(
                    'table'=>'article_comments',
                    'where'=>'article_comment_id = ' . $v->parent_id,
                    'fields'=>'comment'
                );
                $comment_origin = $this->global_model->get_rows($param)->result_array();

                $items[] = array('registrant'=>$v->firstname.' '.$v->lastname,
                                'article_image'=>'../assets/img/articles/resized/50_50_'.$v->article_image,
                                'comment'=>$v->comment,
                                'approval_status'=>$approval_status,
                                'status'=>$v->approval_status,
                                'date_added'=>date('F d, y',strtotime($v->date_added)),
                                'article_name'=>$v->article_name,
                                'id'=>$v->article_comment_id,
                                'comment_origin'=>$comment_origin[0]['comment']
                                );
            }

        }
        $this->response($items,200);


    }

    function article_comments_replies_put(){ 

        $status = $this->put('approval_status');
        $approval_status = array('0'=>'pending','1'=>'approved.','2'=>'disapproved.');
        $this->global_model->update('article_comments',array('approval_status'=>$status),array('article_comment_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully '.$approval_status[$status]),200);

    }

    function article_comments_put(){ 

        $status = $this->put('approval_status');
        $approval_status = array('0'=>'pending','1'=>'approved.','2'=>'disapproved.');
        $this->global_model->update('article_comments',array('approval_status'=>$status),array('article_comment_id'=>$this->put('id')));
        $this->response(array('message'=>'Successfully '.$approval_status[$status]),200);

    }
  




}