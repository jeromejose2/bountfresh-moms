<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require APPPATH.'/libraries/REST_Controller.php';


class Bounty extends REST_Controller {

	private $base_url = '';

	function __construct(){

		parent:: __construct();
		$this->base_url = str_replace('api/','',base_url());

 	}


	function logout_get(){
		$items = array('registrant_id'=>'','registrant_name'=>'','registrant_logged_in'=>'');
		$this->session->unset_userdata($items);
		$this->response(array('message' =>'Successfully logout.','session'=>$this->session->all_userdata()), 200);
	}
	function auth_get(){ 

		$row = $this->global_model->get_row(array('table'=>'registrants',
                                                   'where'=>array('registrant_id'=>$this->session->userdata('registrant_id')),
                                                     )
                                                );
		if($row){
			$this->response(array('error' =>0), 200);
		}else{
			$this->response(array('error' =>1), 200);
		}
 
	}

	function recipes_home_get(){

		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'recipes',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                     'where'=>array('privacy_status'=>1,'featured'=>1),
                                                     'limit'=>1
                                                    )
                                                );
		$items = array();

		if($rows->num_rows()){

			$pages = array('kitchen'=>'The Kitchen','living-room'=>'The Living Room','kids-bedroom'=>'The Kid\'s Bedroom','masters-bedroom'=>'The Master Bedroom');
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$where['recipe_id'] = $v->recipe_id;

				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_recipes',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;
 
				$items[] = array('url_title'=>$v->url_title,
								'title'=>$v->title,
								'description'=>character_limiter($v->description,125),
								'image'=>'assets/img/recipes/'.$v->image,
								'id'=>$v->recipe_id,
								'meal_type'=>$v->meal_type,
								'meal_budget'=>$v->meal_budget,
 								'recipe_page'=>$pages[$v->page],
 								'top_picked'=>$v->top_picked,
 								'favorite'=> $favorite ? 1 : 0
 								);

			}
			
			$this->response($items,200);

		}else
			$this->response(array('error' => 'No results found.'),200);


	}

	function articles_home_get(){


		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'articles',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                    'where'=>array('featured'=>1,'privacy_status'=>1),
                                                    'limit'=>1
                                                    )
                                                );

		if($rows->num_rows()){
			$items = array();
		    $pages = array('kitchen'=>'The Kitchen','living-room'=>'The Living Room','kids-bedroom'=>'The Kid\'s Bedroom','masters-bedroom'=>'The Master Bedroom');
		    $where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$where['article_id'] = $v->article_id;

				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_articles',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;

				$items[]  = array('id'=>$v->article_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/articles/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'promo'=>(int)$v->promo,
								  'author'=>$v->author,
								  'page'=>$v->page,
 								  'date_published'=>date('F d',strtotime($v->date_published)),
								  'article_page'=>$pages[$v->page],
								  'favorite'=> $favorite ? 1 : 0
								  );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}

	function kitchen_recipes_get(){

		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'recipes',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                     'where'=>array('privacy_status'=>1,'page'=>'kitchen')
                                                    )
                                                );
		$items = array();

		if($rows->num_rows()){

			$pages = array('kitchen'=>'The Kitchen','living-room'=>'The Living Room','kids-bedroom'=>'The Kid\'s Bedroom','masters-bedroom'=>'The Master Bedroom');
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			$count = 0;

			foreach($rows->result() as $v){
				$count++;
				$where['recipe_id'] = $v->recipe_id;

				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_recipes',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;
 
				$items[] = array('url_title'=>$v->url_title,
								'title'=>$v->title,
								'description'=>character_limiter(strip_tags($v->description),125),
								'image'=>'assets/img/recipes/'.$v->image,
								'id'=>$v->recipe_id,
								'meal_type'=>$v->meal_type,
								'meal_budget'=>$v->meal_budget,
 								'recipe_page'=>$pages[$v->page],
 								'top_picked'=>$v->top_picked,
 								'date_published'=>date('F d',strtotime($v->date_published)),
 								'favorite'=> $favorite ? 1 : 0,
 								'clear_fix' => $count % 2 == 0 ? 1 : 0,
 								'bug_fix' => $count
 								);

			}
			
			$this->response($items,200);

		}else
			$this->response(array('error' => 'No results found.'),200);


	}

	function kitchen_articles_get(){


		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'articles',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                    'where'=>array('privacy_status'=>1,'page'=>'kitchen')
                                                    )
                                                );

		if($rows->num_rows()){
			$items = array();
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$where['article_id'] = $v->article_id;

				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_articles',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;
				$items[]  = array('id'=>$v->article_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/articles/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'promo'=>(int)$v->promo,
								  'author'=>$v->author,
								  'page'=>$v->page,
 								  'date_published'=>date('F d',strtotime($v->date_published)),
 								  'favorite'=> $favorite ? 1 : 0
 								 );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	} 

	function kitchen_top_picked_articles_get(){

		$this->load->helper('text');
 		$rows = $this->global_model->get_rows(array('table'=>'articles','limit'=>3,'where'=>array('top_picked'=>1,'privacy_status'=>1,'page'=>'kitchen'),'order_by'=>array('field'=>'RAND()','order'=>null)));
	
		
		if($rows){

			$items = array();
			foreach($rows->result() as $v){

				$items[] = array('id'=>$v->article_id,
 								 'image'=>'assets/img/articles/'.$v->image,
								 'url_title'=>$v->url_title,
								 'title'=>$v->title,
								 'description'=>character_limiter(strip_tags($v->description),90)
								 );

			}

			$this->response($items,200);

		}else{

			$this->response(array('error' => 'No results found.'), 200);

		}

	}


	function living_room_articles_get(){


		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'articles',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                    'where'=>array('privacy_status'=>1,'page'=>'living-room')
                                                    )
                                                );

		if($rows->num_rows()){
			$items = array();
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$where['article_id'] = $v->article_id;
				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_articles',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;
				$items[]  = array('id'=>$v->article_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/articles/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'promo'=>(int)$v->promo,
								  'author'=>$v->author,
								  'page'=>$v->page,
 								  'date_published'=>date('F d',strtotime($v->date_published)),
 								  'favorite'=> $favorite ? 1 : 0
 								 );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}


	function living_room_top_picked_articles_get(){

		$this->load->helper('text');
 		$rows = $this->global_model->get_rows(array('table'=>'articles','limit'=>3,'where'=>array('top_picked'=>1,'privacy_status'=>1,'page'=>'living-room'),'order_by'=>array('field'=>'RAND()','order'=>null)));
	
		
		if($rows){

			$items = array();
			foreach($rows->result() as $v){

				$items[] = array('id'=>$v->article_id,
 								 'image'=>'assets/img/articles/resized/296_296_'.$v->image,
								 'url_title'=>$v->url_title,
								 'title'=>$v->title,
								 'description'=>character_limiter(strip_tags($v->description),90)
								 );

			}

			$this->response($items,200);

		}else{

			$this->response(array('error' => 'No results found.'), 200);

		}

	}


	function kids_bedroom_room_articles_get(){


		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'articles',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                    'where'=>array('privacy_status'=>1,'page'=>'kids-bedroom')
                                                    )
                                                );

		if($rows->num_rows()){
			$items = array();
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$where['article_id'] = $v->article_id;
				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_articles',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;
				$items[]  = array('id'=>$v->article_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/articles/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'promo'=>(int)$v->promo,
								  'author'=>$v->author,
								  'page'=>$v->page,
 								  'date_published'=>date('F d',strtotime($v->date_published)),
 								  'favorite'=> $favorite ? 1 : 0
 								 );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}


	function kids_bedroom_top_picked_articles_get(){

		$this->load->helper('text');
 		$rows = $this->global_model->get_rows(array('table'=>'articles','limit'=>3,'where'=>array('top_picked'=>1,'privacy_status'=>1,'page'=>'kids-bedroom'),'order_by'=>array('field'=>'RAND()','order'=>null)));
			
		if($rows){

			$items = array();
			foreach($rows->result() as $v){

				$items[] = array('id'=>$v->article_id,
 								 'image'=>'assets/img/articles/resized/296_296_'.$v->image,
								 'url_title'=>$v->url_title,
								 'title'=>$v->title,
								 'description'=>character_limiter(strip_tags($v->description),90)
								 );

			}

			$this->response($items,200);

		}else{

			$this->response(array('error' => 'No results found.'), 200);

		}

	}


	function master_bedroom_room_articles_get(){


		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'articles',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                    'where'=>array('privacy_status'=>1,'page'=>'masters-bedroom')
                                                    )
                                                );

		if($rows->num_rows()){
			$items = array();
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$where['article_id'] = $v->article_id;
				$favorite = $this->session->userdata('registrant_id') ? $this->global_model->get_row(array('table'=>'favourite_articles',
																									       'where'=>$where
																									       )
																									)
                                                					  : false;
				$items[]  = array('id'=>$v->article_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/articles/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'promo'=>(int)$v->promo,
								  'author'=>$v->author,
								  'page'=>$v->page,
 								  'date_published'=>date('F d',strtotime($v->date_published)),
 								  'favorite'=> $favorite ? 1 : 0
 								 );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}


	function master_bedroom_top_picked_articles_get(){

		$this->load->helper('text');
 		$rows = $this->global_model->get_rows(array('table'=>'articles','limit'=>3,'where'=>array('top_picked'=>1,'privacy_status'=>1,'page'=>'masters-bedroom'),'order_by'=>array('field'=>'RAND()','order'=>null)));
	
		
		if($rows){

			$items = array();
			foreach($rows->result() as $v){

				$items[] = array('id'=>$v->article_id,
 								 'image'=>'assets/img/articles/resized/296_296_'.$v->image,
								 'url_title'=>$v->url_title,
								 'title'=>$v->title,
								 'description'=>character_limiter(strip_tags($v->description),90)
								 );

			}

			$this->response($items,200);

		}else{

			$this->response(array('error' => 'No results found.'), 200);

		}

	}

	 
	function favorite_recipes_get(){

		$this->load->helper('text');
		$filters = $this->get_filters();
		$where = $filters['where_filters'];
		$where['tbl_favourite_recipes.registrant_id'] = $this->session->userdata('registrant_id');
		$join = array(array('table'=>'favourite_recipes','on'=>'tbl_favourite_recipes.recipe_id=tbl_recipes.recipe_id','type'=>'left'),
					  array('table'=>'registrants','on'=>'tbl_registrants.registrant_id=tbl_favourite_recipes.registrant_id','type'=>'right')
					);
		$rows = $this->global_model->get_rows(array('table'=>'recipes',
                                                        'order_by'=>array('field'=>'tbl_recipes.date_added','order'=>'desc'),
                                                        'where'=>$where,
                                                        'join'=>$join,
                                                        'fields'=>'tbl_recipes.*, tbl_favourite_recipes.registrant_id as is_favourite'
                                                        )
                                                );

		if($rows->num_rows()){
			$items = array();
			foreach($rows->result() as $v){
				$items[]  = array('id'=>$v->recipe_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/recipes/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'page'=>$v->page,
								  'is_favourite'=> $v->is_favourite ? true : false,
								  'date_published'=>date('F d',strtotime($v->date_published))
								  );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}

	function favorite_articles_get(){

		$this->load->helper('text');
		$filters = $this->get_filters();
		$where = $filters['where_filters'];
		$where['tbl_favourite_articles.registrant_id'] = $this->session->userdata('registrant_id');
		$join = array(array('table'=>'favourite_articles','on'=>'tbl_favourite_articles.article_id=tbl_articles.article_id','type'=>'left'),
					  array('table'=>'registrants','on'=>'tbl_registrants.registrant_id=tbl_favourite_articles.registrant_id','type'=>'right')
					);
		$rows = $this->global_model->get_rows(array('table'=>'articles',
                                                        'order_by'=>array('field'=>'tbl_articles.date_added','order'=>'desc'),
                                                        'where'=>$where,
                                                        'join'=>$join,
                                                        'fields'=>'tbl_articles.*, tbl_favourite_articles.registrant_id as is_favourite'
                                                        )
                                                );

		if($rows->num_rows()){
			$items = array();
			foreach($rows->result() as $v){
				$items[]  = array('id'=>$v->article_id,
								  'url_title'=>$v->url_title,
								  'image'=>'assets/img/articles/'.$v->image,
								  'title'=>$v->title,
								  'description'=>character_limiter(strip_tags($v->description),125),
								  'featured'=>$v->featured,
								  'top_picked'=>$v->top_picked,
								  'promo'=>(int)$v->promo,
								  'author'=>$v->author,
								  'page'=>$v->page,
								  'is_favourite'=> $v->is_favourite ? true : false,
								  'date_published'=>date('F d',strtotime($v->date_published))
								  );
			}
			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}


	function header_images_get(){
		$where = array('page'=>$this->get('page'));
		$rows = $this->global_model->get_rows(array('table'=>'image_headers','where'=>$where,'order_by'=>array('field'=>'RAND()','order'=>null)));
		$items = array();
		$width = $this->get('width');
		$height = $this->get('height');

		if($rows->num_rows()){
			foreach($rows->result() as $i){

				// if(file_exists('../assets/img/sliders/'.$i->image))
					$items[] = array('image'=>'resize.php?width='.$width.'&height='.$height.'&image=assets/img/sliders/'.$i->image,'link'=>$i->link);
			}
		}

		if($items)
			$this->response($items,200);
		else
			$this->response(array('error' => 'No header found.'), 200);

	}

	function top_picked_articles_get(){

		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'articles','limit'=>3,'where'=>array('top_picked'=>1,'privacy_status'=>1,'page'=>$this->get('page')),'order_by'=>array('field'=>'RAND()','order'=>null)));
		$items = array();

		foreach($rows->result() as $v){
 
			$items[] = array('id'=>$v->article_id,
							'page'=>$v->page,
							'image'=>'assets/img/articles/resized/296_296_'.$v->image,
							'url_title'=>$v->url_title,
							'title'=>$v->title,
							'description'=>character_limiter(strip_tags($v->description),90)
							);

		}

		$this->response($items,200);


	}


	function login_post(){

		$where = array('email'=>$this->post('email'),'password'=>md5($this->post('password')),'status'=>'Confirmed');
		$login = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));

        	if($login) {
        		$this->session->set_userdata(array('registrant_id'=>$login->registrant_id,'registrant_name'=>$login->firstname.'-'.$login->lastname,'registrant_logged_in'=>true));           
	            	$message = array('error'=>0,'message'=>'Successfully logged in.','session'=>$this->session->all_userdata(),'first_visit'=>$login->first_visit); 

	            	if( $login->first_visit ) {
	            		$this->db->where('registrant_id', $login->registrant_id)->update('tbl_registrants', array('first_visit' => 0));
	            	}
        			$this->response($message,200);
	            	return;
        	}

        $where = array('email'=>$this->post('email'),'password'=>md5($this->post('password')));
		$login = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));
        	
        	if($login && $login->status!='Confirmed'){
            	$message = array('error'=>1,'message'=>'Please confirm your registration sent in your email.');
        	} else {
            	$message = array('error'=>1,'message'=>'Invalid username/password. Please try again.');
        	} 
	 
        	$this->response($message,200);
	}

	/* OLD REG PROCESS */
	// function register_post(){

	// 	$this->load->helper('url');

	// 	$_POST = $this->post();
 
	// 	if($this->validate_registration())
	// 	{
	// 		$base_url = $this->base_url;
	// 		$data = $this->post();
 // 			$data['city_id'] = $data['city_id']['city_id'];
	// 		$birthdate = $data['byear'].'-'.$data['bmonth'].'-'.$data['bday'];
	// 		$data['date_added'] = array(array('field'=>'birthdate','value'=>$birthdate),array('field'=>'date_added','value'=>true));
	// 		$data['password'] = md5($data['password']);
 // 			unset($data['agree']);
	// 		$id = $this->global_model->insert('registrants',$data);
	// 		$token = md5($id.'-'.$data['password']);
 // 			$this->global_model->insert('registration_token',array('registrant_id'=>$id,'token'=>$token,'date_added'=>true));
	// 		$link = $base_url.'confirm-registration/'.$token;


	// 		$data['base_url'] = $base_url;		

	// 		$data['message'] = '<p>Hi, '.$data['firstname'].'<br/>Thank you for joining Bounty Fresh Moms! Please confirm your email address by clicking this link: <a href="'.$link.'" target="_blank">'.$link.'</a><br/>
	// 							If you did not sign up for a Bounty Fresh Moms account, please disregard this email.<br/>
	// 							Thank you!<br/><br/>The Bounty Fresh Admin</p>
	// 							   ';
	// 		$email['message'] = $this->load->view('edm',$data,TRUE);
	// 		$email['subject'] = 'Registration Confirmation';
	// 		$email['to'] = $data['email'];
	// 		$email['from'] = 'no-reply@bountyfreshmoms.com';

	// 		if($this->send_edm($email)) 
	// 			$this->response(array('message'=>'Successfully registered. Please confirm your registration by following instructions sent into your email.','error'=>0));
	// 		else
	// 			$this->response(array('message'=>'Successfully registered but confirmation email not sent.','error'=>0));	
	// 	}else{
		
	// 		/*$error_message = explode('.',validation_errors());
	// 		$message = $error_message[0];
	// 		$str = explode(' field', $message);
	// 		$str = explode('The ', $str[0]);
	// 		$error_field = strtolower($str[1]);*/
	// 	    $this->response(array('message'=>validation_errors(),'error'=>1,'error_field'=>0));

	// 	}
		 
	// }

	function update_profile_details_post(){

		$_POST = $this->post();
 
		if($this->validate_update_account())
		{
			$data = $this->post(); 

			$data['city'] = $data['city_id']['city'];
 			$data['province'] = $data['province_id']['province'];
 			$data['city_id'] = $data['city_id']['city_id'];
 			$data['province_id'] = $data['province_id']['province_id'];
			$birthdate = $data['byear'].'-'.$data['bmonth'].'-'.$data['bday'];
			$data['date_modified'] = array(array('field'=>'birthdate','value'=>$birthdate));



			if($this->post('current_password') && $this->post('new_password')){
				$data['password'] = md5($data['new_password']);
			}

			if($this->post('new_password') != ''){
				if($this->post('current_password') != ''){
					$this->global_model->update('registrants',$data,array('registrant_id'=>$this->session->userdata('registrant_id')));

					$row = $this->global_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('registrant_id')) ));
					
					$data['base_url'] = $base_url;
					$data['message'] = '<br><br>This is the summary of your updated profile.<br/><br>
										Firstname: '.$row->firstname.'<br>
										Lastname: '.$row->lastname.'<br>
										Mobile: '.$row->mobile.'<br>
										Email: '.$row->email.'<br>
										Province: '.$row->province.'<br>
										City: '.$row->city.'<br>
										Address: '.$row->address.'<br>
										Birthday: '.date('M d, Y', strtotime($row->birthdate)).'<br>
										Gender: '.$row->gender.'<br><br>
										Thank you!<br/><br/>The Bounty Fresh Admin</p>
										   ';
					$data['firstname'] = $row->firstname;
					$data['lastname'] = $row->lastname;
					
					$email['message'] = $this->load->view('edm',$data,TRUE);
					$email['subject'] = 'Profile Update Confirmation';
					$email['to'] = $row->email;
					$email['from'] = 'no-reply@bountyfreshmoms.com';

					if($this->send_edm($email)) 
						$this->response(array('message'=>'Your profile has been successfully updated.','error'=>0));
					else
						$this->response(array('message'=>'Your profile has been successfully updated but email not sent.','error'=>0));


					// $this->response(array('message'=>'<span class="text-center">Your profile have been successfully updated.</span>','error'=>0));
				}else{
					$this->response(array('message'=>'Please enter your current password.','error'=>1));
				}
			}else{
				$this->global_model->update('registrants',$data,array('registrant_id'=>$this->session->userdata('registrant_id')));

				$row = $this->global_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('registrant_id')) ));
				
				$data['base_url'] = $base_url;
				$data['message'] = '<br><br>This is the summary of your updated profile.<br/><br>
									Firstname: '.$row->firstname.'<br>
									Lastname: '.$row->lastname.'<br>
									Mobile: '.$row->mobile.'<br>
									Email: '.$row->email.'<br>
									Province: '.$row->province.'<br>
									City: '.$row->city.'<br>
									Address: '.$row->address.'<br>
									Birthday: '.date('M d, Y', strtotime($row->birthdate)).'<br>
									Gender: '.$row->gender.'<br><br>
									Thank you!<br/><br/>The Bounty Fresh Admin</p>
									   ';
				$data['firstname'] = $row->firstname;
				$data['lastname'] = $row->lastname;
				
				$email['message'] = $this->load->view('edm',$data,TRUE);
				$email['subject'] = 'Profile Update Confirmation';
				$email['to'] = $row->email;
				$email['from'] = 'no-reply@bountyfreshmoms.com';

				if($this->send_edm($email)) 
					$this->response(array('message'=>'Your profile has been successfully updated.','error'=>0));
				else
					$this->response(array('message'=>'Your profile has been successfully updated but email not sent.','error'=>0));


				// $this->response(array('message'=>'<span class="text-center">Your profile have been successfully updated.</span>','error'=>0));
			}
			

		}else{
			$message = explode('.',validation_errors());
		    $this->response(array('message'=>$message[0],'error'=>1));
		 }

	}

	function alpha_space($str_in) {

		if (! preg_match("/^([-a-z_ñ ])+$/i", $str_in)) {
			$this->form_validation->set_message('alpha_space', 'The %s field may only contain alphabetical characters.');
			return FALSE;
		} else {
			return TRUE;
		}

	}

	function validate_registration(){

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('','');

		$config = array(
               array(
                     'field'   => 'firstname', 
                     'label'   => 'Firstname', 
                     'rules'   => 'required|callback_alpha_space|max_length[50]'
                  ),
               array(
                     'field'   => 'lastname', 
                     'label'   => 'Lastname', 
                     'rules'   => 'required|callback_alpha_space|max_length[50]'
                  ),
               array(
                     'field'   => 'mobile',
                     'label'   => 'Mobile', 
                     'rules'   => 'required|alpha_dash|min_length[11]|max_length[12]'
                  ),   
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'required|valid_email|callback_validate_email'
                  ), 
               array(
                     'field'   => 'province_id', 
                     'label'   => 'Province', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'city_id', 
                     'label'   => 'City', 
                     'rules'   => 'required'
                  ),     
               array(
                     'field'   => 'address', 
                     'label'   => 'Address', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'bday', 
                     'label'   => 'Birthdate', 
                     'rules'   => 'callback_validate_birthdate'
                  ),
               array(
                     'field'   => 'gender', 
                     'label'   => 'Gender', 
                     'rules'   => 'required'
                  )
            );
		
		if($this->session->userdata('registrant_id') && $this->post('current_password')){

			$config[] = array('field'   => 'current_password', 
			                  'label'   => 'Current Password', 
			                  'rules'   => 'required|callback_check_password_exists'
			                  );

			$config[] = array('field'   => 'new_password', 
			                  'label'   => 'New Password', 
			                  'rules'   => 'required|min_length[6]'
			                  );

		}


		 $this->form_validation->set_rules($config);

		return $this->form_validation->run();
	}

	function validate_update_account(){

		$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('','');



		$config = array(
               array(
                     'field'   => 'firstname', 
                     'label'   => 'Firstname', 
                     'rules'   => 'required|callback_alpha_space|max_length[50]'
                  ),
               array(
                     'field'   => 'lastname', 
                     'label'   => 'Lastname', 
                     'rules'   => 'required|callback_alpha_space|max_length[50]'
                  ),
               array(
                     'field'   => 'mobile',
                     'label'   => 'Mobile', 
                     'rules'   => 'required|alpha_dash|min_length[11]|max_length[12]'
                  ),   
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'required|valid_email'
                  ), 
               array(
                     'field'   => 'province_id', 
                     'label'   => 'Province', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'city_id', 
                     'label'   => 'City', 
                     'rules'   => 'required'
                  ),     
               array(
                     'field'   => 'address', 
                     'label'   => 'Address', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'bday', 
                     'label'   => 'Birthdate', 
                     'rules'   => 'callback_validate_birthdate'
                  ),
               array(
                     'field'   => 'gender', 
                     'label'   => 'Gender', 
                     'rules'   => 'required'
                  )
            );
		
		if($this->session->userdata('registrant_id') && $this->post('current_password')){

			$config[] = array('field'   => 'current_password', 
			                  'label'   => 'Current Password', 
			                  'rules'   => 'required|callback_check_password_exists'
			                  );

			$config[] = array('field'   => 'new_password', 
			                  'label'   => 'New Password', 
			                  'rules'   => 'required|min_length[6]|callback_check_password_same'
			                  );

		}else if(!$this->session->userdata('registrant_id')){

			$config[] = array('field'   => 'password', 
			                  'label'   => 'Password', 
			                  'rules'   => 'required'
			                  );
		}


		 $this->form_validation->set_rules($config);

		return $this->form_validation->run();
	}

	function check_password_exists($pwd){
		$where = array('registrant_id'=>$this->session->userdata('registrant_id'),'password'=>md5($pwd));
        $row = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));

        if(!$row){
        	$this->form_validation->set_message('check_password_exists','Invalid current password.');
			return false;
        }
        return true;
	}

	function check_password_same($pwd){
		$where = array('registrant_id'=>$this->session->userdata('registrant_id'),'password'=>md5($pwd));
        $row = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));

        if($row){
        	$this->form_validation->set_message('check_password_same','New password cannot be the same as current password.');
			return false;
        }
        return true;
	}

	 

	function validate_birthdate(){
		$bmonth = $this->post('bmonth');
		$bday = $this->post('bday');
		$byear = $this->post('byear');

		if(!checkdate($bmonth,$bday,$byear)){
			$this->form_validation->set_message('validate_birthdate','The birthdate field is invalid.');
			return false;
		}
		return true;

	}


	function upload_profile_post(){
		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = '../assets/img/registrants';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2048','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);	    
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>62,'height'=>62,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>102,'height'=>102,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $response['new_image'] = $file['file_name'];

	    }else
	    	$response['error'] = $file;
	    
 	    $this->response($response);

	}


	function user_get(){
		
		$where = array('registrant_id'=>$this->session->userdata('registrant_id'));
		$response = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));
		$user = array();
		if($response){

			$edit_profile_photo = file_exists('../assets/img/registrants/resized/102_102_'.$response->image) ? 'assets/img/registrants/resized/102_102_'.$response->image : 'assets/img/upload-default-big.jpg';
			$user = array('firstname'=>$response->firstname,
							'lastname'=>$response->lastname,
							'mobile'=>$response->mobile,
							'email'=>$response->email,
							'address'=>$response->address,
							'bmonth'=>date('n',strtotime($response->birthdate)),
							'bday'=>date('j',strtotime($response->birthdate)),
							'byear'=>date('Y',strtotime($response->birthdate)),
							'gender'=>$response->gender,
							'image'=>$response->image,
							'province_id'=>$response->province_id,
							'city_id'=>$response->city_id,
							'edit_profile_photo'=>$edit_profile_photo);

			$this->response($user,200);

		}else $this->response(array('message'=>'User not found'));

	}


	function recipe_get(){

		$where = array('recipe_id'=>$this->get('id'));
 		$row = $this->global_model->get_row(array('table'=>'recipes as r','where'=>$where,'limit'=>1));
		$recipe = array('message'=>'Recipe not found.');
		$previous = false;
		$next = false;
		
		if($row){

			$fav = $this->global_model->get_row(array('table'=>'favourite_recipes','fields'=>'favourite_id','where'=>array('recipe_id'=>$this->get('id'),'registrant_id'=>$this->session->userdata('registrant_id'))));
			$recipe = array('id'=>$row->recipe_id,
								'title'=>$row->title,
                                                             'description'=>$row->description,
								//'description'=>strip_tags(substr($row->description,0,3000))  . "...",//html_entity_decode(htmlentities($row->description)),
								'image'=>'assets/img/recipes/'.$row->image,
								'url_title'=>$row->url_title,
								'is_favourite'=>($fav) ? true : false,
								'date_published'=>date('F d',strtotime($row->date_published)),
								'back_page'=>"Back to The " . ucfirst($row->page)
								);
			$prev = $this->global_model->get_row(array('table'=>'recipes','fields'=>'recipe_id as id,url_title','limit'=>1,'where'=>array('recipe_id >'=>$row->recipe_id,'privacy_status'=>1)));
			$next = $this->global_model->get_row(array('table'=>'recipes','fields'=>'recipe_id as id,url_title','limit'=>1,'where'=>array('recipe_id <'=>$row->recipe_id,'privacy_status'=>1)));

		}

		$response = array('recipe'=>$recipe,'previous_recipe'=>(array)$prev,'next_recipe'=>(array)$next);
		$this->response($response,200);

	}

	function favourite_recipe_post(){

		$where = array('recipe_id'=>$this->post('id'),'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'recipes','where'=>$where));
 
		if($row && $this->session->userdata('registrant_id')){

			$where = array('recipe_id'=>$this->post('id'),'registrant_id'=>$this->session->userdata('registrant_id'));
			$fav = $this->global_model->get_row(array('table'=>'favourite_recipes','where'=>$where));

			if(!$fav){
				$this->global_model->insert('favourite_recipes',array('recipe_id'=>$this->post('id'),'date_added'=>true,'registrant_id'=>$this->session->userdata('registrant_id')));
				$this->response(array('message'=>'Successfully added as your favourite recipe.'),200);
			}else{
				$this->response(array('message'=>'Already added as your favourite recipe.'),200);
			} 

		}else if(!$this->session->userdata('registrant_id'))
			$this->response(array('message'=>'Please login to continue.'),200);
		else
			$this->response(array('message'=>'Recipe not found.'),200);


	}


	function recipe_comments_post(){

		$recipe_id = $this->post('id');
		$parent_id = $this->post('parent_id');
		$user_id = $this->session->userdata('registrant_id');
		$comment = ((int)$parent_id) ? $this->post('comment_reply') : $this->post('comment');

		$where = array('recipe_id'=>$recipe_id,'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'recipes','where'=>$where));

		if($row && $user_id){

			$data = array('recipe_id'=>$recipe_id,'registrant_id'=>$user_id,'parent_id'=>$parent_id,'comment'=>$comment,'date_added'=>true,'approval_status'=>0);
			$this->global_model->insert('recipe_comments',$data);
			$this->response(array('message'=>'Your comment is subject for approval.<br/>Thank you!'),200);

		}else{
			$this->response(array('message'=>'Please login to your account to continue.'),200);
		}

	}

	function recipe_comments_get(){

		$where = array('recipe_id'=>$this->get('id'),'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'recipes','where'=>$where));

		if($row){
			$params =  array('table'=>'tbl_recipe_comments',
							'where'=>array('tbl_recipe_comments.recipe_id'=>$this->get('id'),'tbl_recipe_comments.parent_id'=>0,'tbl_recipe_comments.approval_status'=>1),
							'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=tbl_recipe_comments.registrant_id','type'=>'left')),
							'fields'=>'tbl_recipe_comments.*, r.image as profile_image, r.firstname as firstname, r.lastname as lastname',
							'order_by'=>array('field'=>'tbl_recipe_comments.date_added','order'=>'ASC')		
							);
 			$rows = $this->global_model->get_rows($params);
 			$items = array();
 			foreach ($rows->result() as $v) {
 				$profile_image = file_exists('../assets/img/registrants/resized/62_62_'.$v->profile_image) ? 'assets/img/registrants/resized/62_62_'.$v->profile_image : 'assets/img/default-comment-profile-photo.jpg';
 				$items[] = array('id'=>$v->recipe_comment_id,'profile_image'=>$profile_image,'firstname'=>$v->firstname,'lastname'=>$v->lastname,'comment'=>$v->comment,'date_added'=>date('F d, Y',strtotime($v->date_added)).' at '.date('h:i a',strtotime($v->date_added)),'parent_id'=>$v->parent_id);
 			}
		 	$this->response($items,200);
		}else
			$this->response(array('message'=>'not found'),200);

	}

	function recipe_comment_replies_get(){

		$params =  array('table'=>'tbl_recipe_comments',
						'where'=>array('tbl_recipe_comments.parent_id'=>$this->get('id'),'tbl_recipe_comments.approval_status'=>1),
						'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=tbl_recipe_comments.registrant_id','type'=>'left')),
						'fields'=>'tbl_recipe_comments.*, r.image as profile_image, r.firstname as firstname, r.lastname as lastname',
						'order_by'=>array('field'=>'tbl_recipe_comments.date_added','order'=>'ASC')		
						);
		$items = $this->global_model->get_rows($params);
		$this->response($items->result_array(),200);

	}



	function article_get(){

		$this->load->helper('inflector');

		$where = array('article_id'=>$this->get('id'),'privacy_status'=>1);
 		$row = $this->global_model->get_row(array('table'=>'articles','where'=>$where,'limit'=>1));
 		$total_rows = $this->global_model->get_rows(array('table'=>'articles', 'where'=>array('page'=>$row->page, 'privacy_status'=>1)));
		$response = array('message'=>'Article not found.');
		$article = array();
		$previous = false;
		$next = false;
		$cur_page = (int) $this->article_current_page($this->get('id'), $total_rows->result_array()) + 1;
		
		if($row){

			$fav = $this->global_model->get_row(array('table'=>'favourite_articles','fields'=>'favourite_id','where'=>array('article_id'=>$this->get('id'),'registrant_id'=>$this->session->userdata('registrant_id'))));
			$article = array('id'=>$row->article_id,
							  'url_title'=>$row->url_title,
							  'image'=>'assets/img/articles/'.$row->image,
							  'title'=>$row->title,
							 //  'description'=>$row->description ,
							  'description'=>strip_tags(substr($row->description,0,3000)) . "...",
							  'featured'=>$row->featured,
							  'top_picked'=>$row->top_picked,
							  'promo'=>$row->promo,
							  'author'=>$row->author,
							  'page'=>$row->page,
							  'back_page'=>"Back to The " . humanize(str_replace(" ", "_", str_replace("-", " ", $row->page))),
							  'date_published'=>date('F d',strtotime($row->date_published)),
								'is_favourite'=>($fav) ? true : false,
								'total_items' => $total_rows->num_rows(),
								'cur_page' => $cur_page
								);

			$previous = $this->global_model->get_rows(array('table'=>'articles',
															'fields'=>'article_id as id,url_title',
															'limit'=>1,
															'where'=>array('article_id <'=>$row->article_id,'privacy_status'=>1,'page'=>$row->page),
															'order_by'=>array('field'=>'id','order'=>'DESC')
															)
													)->row();

			$next = $this->global_model->get_rows(array('table'=>'articles',
														'fields'=>'article_id as id,url_title',
														'limit'=>1,
														'where'=>array('article_id >'=>$row->article_id,'privacy_status'=>1,'page'=>$row->page),
														'order_by'=>array('field'=>'id','order'=>'ASC')
														)
												)->row();

		}

		$response = array('article'=>$article,'previous_article'=>(array)$previous,'next_article'=>(array)$next);
		$this->response($response,200);
		// $this->output->enable_profiler(TRUE);

	}

	function article_current_page($id, $array) {

		foreach($array as $key => $val) {
			if($val['article_id'] === $id) {
				return $key;
			}
		}
		return NULL;
	}

	function favourite_article_post(){

		$where = array('article_id'=>$this->post('id'),'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'articles','where'=>$where));

		if($row && $this->session->userdata('registrant_id')){

			$where = array('article_id'=>$this->post('id'),'registrant_id'=>$this->session->userdata('registrant_id'));
			$fav = $this->global_model->get_row(array('table'=>'favourite_articles','where'=>$where));

			if(!$fav){
				$this->global_model->insert('favourite_articles',array('article_id'=>$this->post('id'),'date_added'=>true,'registrant_id'=>$this->session->userdata('registrant_id')));
				$this->response(array('message'=>'Successfully added as your favourite article.'),200);
			}else{
				$this->response(array('message'=>'Already added as your favourite article.'),200);

			}

		}else if(!$this->session->userdata('registrant_id'))
			$this->response(array('message'=>'Please login to continue.'),200);
		else
			$this->response(array('message'=>'Article not found.'),200);
	}


	function remove_favourite_article_post(){

		$where = array('article_id'=>$this->post('id'),'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'articles','where'=>$where));

		if($row){

			$where = array('article_id'=>$this->post('id'),'registrant_id'=>$this->session->userdata('registrant_id'));
			$fav = $this->global_model->get_row(array('table'=>'favourite_articles','where'=>$where));

			if($fav){
				$this->global_model->delete('favourite_articles',array('article_id'=>$this->post('id')));
				$this->response(array('message'=>'You have successfully removed the article.'),200);
			}else{
				$this->response(array('message'=>'Already removed.'),200);

			}

			

		}else
			$this->response(array('message'=>'Article not found.'),200);


	}

	function remove_favourite_recipe_post(){

		$where = array('recipe_id'=>$this->post('id'),'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'recipes','where'=>$where));

		if($row){

			$where = array('recipe_id'=>$this->post('id'),'registrant_id'=>$this->session->userdata('registrant_id'));
			$fav = $this->global_model->get_row(array('table'=>'favourite_recipes','where'=>$where));

			if($fav){
				$this->global_model->delete('favourite_recipes',array('recipe_id'=>$this->post('id')));
				$this->response(array('message'=>'You have successfully removed the recipe.'),200);
			}else{
				$this->response(array('message'=>'Already removed.'),200);

			}

			

		}else
			$this->response(array('message'=>'Recipe not found.'),200);


	}


	function article_comments_post(){

		$article_id = $this->post('id');
		$parent_id = $this->post('parent_id');
		$user_id = $this->session->userdata('registrant_id');
		$comment = ((int)$parent_id) ? $this->post('comment_reply') : $this->post('comment');

		$where = array('article_id'=>$article_id,'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'articles','where'=>$where));

		if($row && $user_id && strlen($comment) > 0){

			$data = array('article_id'=>$article_id,'registrant_id'=>$user_id,'parent_id'=>$parent_id,'comment'=>$comment,'date_added'=>true,'approval_status'=>0);
			$this->global_model->insert('article_comments',$data);
			$this->response(array('message'=>'Your comment is subject for approval. Thank you!'),200);

		}else{
			$this->response(array('message'=>'Please login to continue.'),200);
		}

	}

	function article_comments_get(){

		$where = array('article_id'=>$this->get('id'),'privacy_status'=>1);
		$row = $this->global_model->get_row(array('table'=>'articles','where'=>$where));
		$user_id = $this->session->userdata('registrant_id');

		if($row){
			$params =  array('table'=>'tbl_article_comments',
							'where'=>array('tbl_article_comments.article_id'=>$this->get('id'),'tbl_article_comments.parent_id'=>0,'tbl_article_comments.approval_status'=>1),
							'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=tbl_article_comments.registrant_id','type'=>'left')),
							'fields'=>'tbl_article_comments.*, r.image as profile_image, r.firstname as firstname, r.lastname as lastname',
							'order_by'=>array('field'=>'tbl_article_comments.date_added','order'=>'ASC')		
							);
 			$rows = $this->global_model->get_rows($params);
 			$items = array();
 			foreach ($rows->result() as $v) {
 				$profile_image = $v->profile_image ? 'assets/img/registrants/resized/62_62_'.$v->profile_image : 'assets/img/default-comment-profile-photo.jpg';
 				$items[] = array('id'=>$v->article_comment_id,'profile_image'=>$profile_image,'firstname'=>$v->firstname,'lastname'=>$v->lastname,'comment'=>$v->comment,'date_added'=>date('F d, Y',strtotime($v->date_added)).' at '.date('h:i a',strtotime($v->date_added)),'parent_id'=>$v->parent_id);
 			}
		 	$this->response($items,200);
		}else
			$this->response(array('message'=>'not found'),200);

	}

	function article_comment_replies_get(){

		$params =  array('table'=>'tbl_article_comments',
						'where'=>array('tbl_article_comments.parent_id'=>$this->get('id'),'tbl_article_comments.approval_status'=>1),
						'join'=>array(array('table'=>'tbl_registrants as r','on'=>'r.registrant_id=tbl_article_comments.registrant_id','type'=>'left')),
						'fields'=>'tbl_article_comments.*, r.image as profile_image',
						'order_by'=>array('field'=>'tbl_article_comments.date_added','order'=>'ASC')		
						);
		$items = $this->global_model->get_rows($params);
		
		$this->response($items->result_array(),200);

	}

	function related_articles_get(){
		$this->load->helper('text');
		$where = array('page !='=>$this->get('page'));
 		$rows = $this->global_model->get_rows(array('fields'=>'article_id as id, title, url_title, description,image','table'=>'articles','where'=>$where,'limit'=>4,'order_by'=>array('field'=>'RAND()','order'=>null)));
 		$items = array();
 		if($rows->num_rows()){
 			foreach($rows->result() as $v){
 				$items[] = array('id'=>$v->id,
 								 'title'=>$v->title,
 								 'url_title'=>$v->url_title,
 								 'description'=>character_limiter($v->description,110),
 								 'image'=>'assets/img/articles/'.$v->image);
 			}
 		}
 		$this->response($items,200);
	}

	
	function get_filters(){
		   
		   $where_filters = array('tbl_recipes.featured'=>'rfeatured',
		   						  'tbl_articles.featured'=>'afeatured',
		   						  'tbl_articles.page'=>'apage',
		   						  'tbl_recipes.page'=>'rpage');

		   $like_filters = array();

		   $query_strings = array();
		   
		   $valid_where_filters = array();
		   $valid_like_filters = array();
		   
		   foreach($where_filters as $column_field=>$filter){
			    
 				if($this->get($filter) || $this->get($filter) ==='0'){
 					$valid_where_filters[$column_field] = trim($this->get($filter));
 					$query_strings[$filter] = $this->get($filter);
 				}
									 
				  
 		   } 
		    
		   
		   foreach($like_filters as $column_field=>$filter){
			   
 				if($this->get($filter)){
 					$valid_like_filters[$column_field] = trim($this->get($filter));
 					$query_strings[$filter] = $this->input->get($filter);
 				}
					
				 
				   
		   } 

 		   $query_strings = ($query_strings) ? http_build_query($query_strings) : '';
 		   
		   return array('where_filters'=>$valid_where_filters,'like_filters'=>$valid_like_filters,'query_strings'=>'?'.$query_strings);		   
		   
	} 

   function send_edm($param){

//	$ci =& get_instance();
//	$ci->load->library('email');
//	$config['protocol'] = 'smtp';
//	$config['smtp_host'] = 'ssl://smtp.gmail.com';
//	$config['smtp_port'] = '465';
	// $config['smtp_user'] = 'jeffrey.mabazza@nuworks.ph';
	// $config['smtp_pass'] = 'jeff-m@b&Q!';
//	$config['smtp_user'] = 'bountyfresh.marketings@gmail.com';
//	$config['smtp_pass'] = 'bfmarketingp@ssw0rd';
//	$config['charset'] = 'utf-8';
//	$config['mailtype'] = 'html';
//	$config['newline'] = "\r\n";

//	$ci->email->initialize($config);

//	$ci->email->from($param['from'], 'Bounty Fresh Admin');
//	$ci->email->to($param['to']);
//	$ci->email->subject($param['subject']);
//	$ci->email->message($param['message']);
//
//	if($ci->email->send()) {
//		return true;
//	} else {
//		return false;
//	}


  		$this->load->library('email');

 	$config['protocol'] = 'sendmail';
 $config['mailpath'] = '/usr/sbin/sendmail';
 $config['charset'] = 'iso-8859-1';
 $config['wordwrap'] = TRUE;
 $config['mailtype'] = 'html';

 $this->email->initialize($config);	

 $this->email->from($param['from']);
 $this->email->to($param['to']); 
 $this->email->subject($param['subject']);
 $this->email->message($param['message']); 

 if($this->email->send())
 	return true;
 else
 	return false;

   }


	function send_contact_us_form_post(){

		
		$_POST = $this->post();
		$_POST['firstname'] = $this->post('firstname');

		if($this->validate_contac_us_form()){

			$this->load->library('email');			

		 	$config['protocol'] = 'sendmail';
			$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = 'html';

			$this->email->initialize($config);	 	

			$this->email->from('no-reply@bountyfreshmoms.com');
			$this->email->to('nuworks.client1@gmail.com, customercare@bountyagro.com.ph, bountyfresh.marketing@gmail.com'); 
	 
			$this->email->subject('Inquery for ChicMoms Club');
			$message = '<p><strong>Name</strong>: '.$this->post('firstname').' '.$this->post('lastname');
			$message .= '<br/><p><strong>Email</strong>: '.$this->post('subject');
			$message .= '<br/><p><strong>Subject</strong>: '.$this->post('subject');
			$message .= '<br/><p><strong>Description</strong>: '.$this->post('message');
			$this->email->message($message); 

			if($this->email->send())
				$this->response(array('message'=>'Thank you! Your message has been sent!','error'=>0),200);
			else
				$this->response(array('message'=>'Something went wrong. Failed to send your inquiry.','error'=>1),200);

		}else{

			$this->response(array('message'=>validation_errors(),'error'=>1),200);
			
		}

	 	
	}

 

	function validate_contac_us_form(){

		

		$this->load->library('form_validation');
 
		$config = array(
               array(
                     'field'   => 'firstname', 
                     'label'   => 'Firstname', 
                     'rules'   => 'required|alpha_space|max_length[50]'
                  ),
               array(
                     'field'   => 'lastname', 
                     'label'   => 'Lastname', 
                     'rules'   => 'required|alpha_space|max_length[50]'
                  ),  
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'required|valid_email'
                  ), 
               array(
                     'field'   => 'subject', 
                     'label'   => 'Subject', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'message', 
                     'label'   => 'Message', 
                     'rules'   => 'required'
                  )
            );

		 $this->form_validation->set_rules($config);

		return $this->form_validation->run();
	}

	 

	function provinces_get(){

		$rows = $this->global_model->get_rows(array('table'=>'provinces','order_by'=>array('field'=>'province','order'=>'asc')));

		$index = null;
		if($this->input->get('province_id')){

			foreach($rows->result() as $key=>$v){
				if($this->input->get('province_id')==$v->province_id){
					$index = $key;
					break;
				}
			}

		}
		$this->response(array('provinces'=>$rows->result_array(),'index'=>$index),200);
	}

	function cities_get(){

		$where = array('province_id'=>strtolower($this->get('province')));
		$rows = $this->global_model->get_rows(array('where'=>$where,'table'=>'cities','order_by'=>array('field'=>'city','order'=>'ASC')));
		$index = -1;
		if((int)$this->input->get('city_id')){
			$city_id = $this->input->get('city_id');

			foreach($rows->result() as $key=>$v){
				if($city_id==$v->city_id){
					$index = $key;
					break;
				}
			}

		}
		$this->response(array('cities'=>$rows->result_array(),'index'=>$index),200);
	}

	/* OLD REG PROCESS */
	// function confirm_registration_post(){

	// 	$where = array('token'=>$this->post('token'),'status !='=>'Confirmed');
	// 	$row = $this->global_model->get_row(array('table'=>'registration_token','where'=>$where));

	// 	if($row){

	// 		$this->global_model->update('registration_token',array('status'=>'Confirmed','date_modified'=>array(array('field'=>'date_confirmed','value'=>'NOW()'))),array('registrant_id'=>$row->registrant_id));

	// 		$where = array('registrant_id'=>$row->registrant_id);
	// 		$row = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));


	// 		$this->global_model->update('registrants',array('status'=>'Confirmed', 'date_confirmed'=>date('Y-m-d h:i:s')),array('registrant_id'=>$row->registrant_id));

	// 		//$this->session->set_userdata(array('registrant_id'=>$row->registrant_id,'registrant_name'=>$row->firstname.'-'.$row->lastname,'registrant_logged_in'=>true));           

	// 		$this->response(array('status'=>'success','message'=>'Your registration has been successfully confirmed. You may now login to your account.'),200);
	// 	}else{
	// 		$this->response(array('status'=>'failed','message'=>'Confirmation token is not found/active.','token'=>$this->post('token')),200);
	// 	}


	// }

	function request_new_password_post(){

		$where = array('email'=>$this->post('email'));
		$row = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));

		if($row){

			$base_url = $this->base_url;
			$token = md5($row->registrant_id.'-'.rand());
			
			$link = $base_url.'/reset-password/'.$token;
			$data['base_url'] = $base_url;
			$data['firstname'] = $row->firstname;
			$data['lastname'] = '';
			$data['message'] = '<p>You have requested a new password in Bounty Fresh Moms Website.<br/><br/>
								   Please click <a href="'.$link.'" target="_blank">here</a> to reset your password.<br/><br/>
								    Please ignore this email if you didn\'t request.<br/><br/>
								    </p><br/><br/>
								   <big>Thank you,<big>
								   <p>Admin</p>';
			$email['message'] = $this->load->view('edm',$data,TRUE);
			$email['subject'] = 'Request New Password';
			// $email['to'] = $this->post('email');
			$email['to'] = $row->email;
			$email['from'] = 'no-reply@bountyfreshmoms.com';

			$this->send_edm($email);

			$this->global_model->insert('request_password',array('token'=>$token,'status'=>'active','registrant_id'=>$row->registrant_id,'date_added'=>true));

			$this->response(array('status'=>1,'message'=>'Email sent for your new password request.'),200);

		}else{
			$this->response(array('status'=>0,'message'=>'Email not found.'),200);
		}
		
	}
 

	function reset_password_post(){

		$where = array('token'=>$this->post('token'),'status'=>'active');
		$row = $this->global_model->get_row(array('table'=>'request_password','where'=>$where));
		$password = $this->post('password');
		$cpassword = $this->post('cpassword');

		if($row){

			if($password == $cpassword && strlen($password) >= 6){

				$this->global_model->update('request_password',array('status'=>'inactive'),array('reset_id'=>$row->reset_id));
				$this->global_model->update('registrants',array('password'=>md5($password)),array('registrant_id'=>$row->registrant_id));
				$this->response(array('status'=>'success','message'=>'Your password has been successfully reset. You may now login to your account.'),200);

			}else{

				$message = strlen($password) < 6 ? 'Your password must have at least 6 characters in length' : 'Password not match';
				$this->response(array('status'=>'failed','message'=>$message),200);
			}

		}else{

			$this->response(array('status'=>'failed','message'=>'Request password token is not found/active.'),200);

		}

	}


	function meal_types_get(){

		$rows = $this->global_model->get_rows(array('table'=>'recipes',
													'fields'=>'meal_type',
													'order_by'=>array('field'=>'meal_type','order'=>'desc'),
													'where'=>array('meal_type !='=>''),
													'group_by'=>'meal_type'
													)
											);
		$this->response($rows->result_array(),200);

	}

	function meal_budgets_get(){

		$rows = $this->global_model->get_rows(array('table'=>'recipes',
													'fields'=>'meal_budget',
													'order_by'=>array('field'=>'meal_budget','order'=>'desc'),
													'where'=>array('meal_budget !='=>''),
													'group_by'=>'meal_budget'
													)
											);
		$this->response($rows->result_array(),200);

	}

	function faq_get(){

		$rows = (array)$this->global_model->get_row(array('table'=>'faq','where'=>array('faq_id'=>1)) );
		$this->response($rows,200);

	}

	function contact_details_bff_get(){

		$rows = (array)$this->global_model->get_row(array('table'=>'contact_details','where'=>array('contact_details_id'=>1)) );
		$this->response($rows,200);

	}

	function contact_details_bav_get(){

		$rows = (array)$this->global_model->get_row(array('table'=>'contact_details','where'=>array('contact_details_id'=>2)) );
		$this->response($rows,200);

	}
	

	function privacy_policy_get(){

		$rows = (array)$this->global_model->get_row(array('table'=>'privacy_policy','where'=>array('privacy_id'=>1)) );
		$this->response($rows,200);

	}

	function terms_and_conditions_get(){

		$rows = (array)$this->global_model->get_row(array('table'=>'terms_and_conditions','where'=>array('terms_and_conditions_id'=>1)) );
		$this->response($rows,200);

	}

	function validate_email($email) {

		$row = $this->global_model->get_row(array('table'=>'tbl_registrants','where'=>array('email'=>$email, 'status'=>'Confirmed')) );
		if($row) {
			$this->form_validation->set_message('validate_email', 'Email already exist.');
			return false;
		}

		return true;

	}

	/*NEW REG PROCESS*/
	function register_post(){

		$this->load->helper('url');

		$_POST = $this->post();
 
		if($this->validate_registration())
		{
			$base_url = $this->base_url;
			$data = $this->post();
 			$data['city'] = $data['city_id']['city'];
 			$row = $this->global_model->get_row(array('table'=>'tbl_provinces','where'=>array('province_id'=>$data['province_id']) ));
 			$data['province'] = $row->province;
 			$data['city_id'] = $data['city_id']['city_id'];
 			$data['province_id'] = $data['province_id'];
			$birthdate = $data['byear'].'-'.$data['bmonth'].'-'.$data['bday'];
			$data['date_added'] = array(array('field'=>'birthdate','value'=>$birthdate),array('field'=>'date_added','value'=>true));
			$data['password'] = md5($data['password']);
 			unset($data['agree']);
			$id = $this->global_model->insert('registrants',$data);
			$token = md5($id.'-'.$data['password']);
 			$this->global_model->insert('registration_token',array('registrant_id'=>$id,'token'=>$token,'date_added'=>true));
			$link = $base_url.'api/registration/confirm_registration/'.$token;
			// $link = 'http://chicmoms.storyteching.ph/api/registration/confirm_registration/'.$token;
			
			$data['base_url'] = $base_url;		

			$data['message'] = '<p>Thank you for joining Bounty Fresh Moms! Please confirm your email address by clicking this link: <a href="'.$link.'" target="_blank">Click here!</a><br/>
								If you did not sign up for a Bounty Fresh Moms account, please disregard this email.<br/> <br><br>
								Thank you!<br/>The Bounty Fresh Admin</p>
								   ';
			$email['message'] = $this->load->view('edm',$data,TRUE);
			$email['subject'] = 'Registration Confirmation';
			$email['to'] = $data['email'];
			$email['from'] = 'no-reply@bountyfreshmoms.com';

			if($this->send_edm($email)) 
				$this->response(array('message'=>'Successfully registered. Please confirm your registration by following instructions sent into your email.','error'=>0));
			else
				$this->response(array('message'=>'Successfully registered but confirmation email not sent.','error'=>0));	
		}else{
		
			/*$error_message = explode('.',validation_errors());
			$message = $error_message[0];
			$str = explode(' field', $message);
			$str = explode('The ', $str[0]);
			$error_field = strtolower($str[1]);*/
		    $this->response(array('message'=>validation_errors(),'error'=>1,'error_field'=>0));

		}
		 
	}

	function pledge_post(){

		$this->load->helper('url');

		$_POST = $this->post();
 
		if($this->validate_pledge())
		{
			$base_url = $this->base_url;
			$data = $this->post();
			$data['registrant_id'] = $this->session->userdata('registrant_id');
 			$data['category'] = $data['catagory']['text'];
 			$data['type'] = strtolower($data['type']['text']);
 			$data['message'] = $data['commentBox'];

			$this->global_model->insert('pledge',$data);


			$row = $this->global_model->get_row(array('table'=>'tbl_registrants','where'=>array('registrant_id'=>$this->session->userdata('registrant_id')) ));
			
			$data['base_url'] = $base_url;
			$data['message'] = '<p>Hi '.$row->firstname.', <br/>Thank you for pledging! Below is the copy of your answers.<br/>
								#DiskarteNaMayArte sa: '.$data['catagory']['text'].'<br>
								Special Touch: '.$data['type'].'<br>
								Specialty: '.$data['commentBox'].' <br><br>
								Thank you!<br/>The Bounty Fresh Admin</p>
								   ';
			$data['firstname'] = $row->firstname;
			$data['lastname'] = $row->lastname;
			
			$email['message'] = $this->load->view('edm',$data,TRUE);
			$email['subject'] = 'Pledge Confirmation';
			$email['to'] = $row->email;
			$email['from'] = 'no-reply@bountyfreshmoms.com';

			if($this->send_edm($email)) 
				$this->response(array('message'=>'Successfully pledged!','error'=>0));
			else
				$this->response(array('message'=>'Successfully pledged but email not sent.','error'=>0));	

				
		}else{
		    $this->response(array('message'=>validation_errors(),'error'=>1,'error_field'=>0));
		}
		 
	}

	function validate_pledge(){

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('','');

		$config = array(
               array(
                     'field'   => 'catagory', 
                     'label'   => 'category', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'type',  
                     'rules'   => 'required|callback_check_pledge_exists'
                  ),
               array(
                     'field'   => 'nickname',
                     'rules'   => 'required'
                  )
             
            );

		 $this->form_validation->set_rules($config);

		return $this->form_validation->run();
	}

	function check_pledge_exists($type){

		$this->load->helper('url');

		$type = $type['text'];
	
		$where = array('registrant_id'=>$this->session->userdata('registrant_id'),'type'=>$type);
        $row = $this->global_model->get_row(array('table'=>'pledge','where'=>$where));

        if($row){
        	$this->form_validation->set_message('check_pledge_exists','You already pledged under this category! Pero pwede ka pang mag-pledge sa iba. Dali, share more #DiskarteNaMayArte tips with us!');
			return false;
        }
        return true;
	}

	function pledge_list_get(){


		$this->load->helper('text');
		$rows = $this->global_model->get_rows(array('table'=>'pledge',
                                                    'order_by'=>array('field'=>'date_added','order'=>'desc'),
                                                    'where'=>array('registrant_id'=>$this->session->userdata('registrant_id'))
                                                    )
                                                );

		if($rows->num_rows()){
			$items = array();
			$where = array('registrant_id'=>$this->session->userdata('registrant_id'));

			foreach($rows->result() as $v){

				$items[]  = array('id'=> $v->id,
								  'registrant_id'=> $v->registrant_id,
								  'category'=> $v->category,
								  'type'=> strtolower($v->type),
								  'message'=> $v->message,
								  'nickname'=>$v->nickname,
								  'date_added'=> date('M d, Y g:i:s', strtotime($v->date_added))
 								 );
			}

			$this->response($items,200);
		}else
			$this->response(array('error' => 'No results found.'), 200);

	}

}
