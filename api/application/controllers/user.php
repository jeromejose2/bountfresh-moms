<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User extends CI_Controller {


	function login()
	{ 

		$where = array('email'=>$this->input->post('email'),'password'=>md5($this->input->post('password')));
        $login = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));
         
        if($login){
            $message = array('logged_in'=>true,'login_message'=>'Successfully logged in.'); 
            $this->session->set_userdata(array('registrant_id'=>$login->registrant_id,'registrant_name'=>$login->name,'registrant_logged_in'=>true));           
        }else{
            $message = array('logged_in'=>false,'login_message'=>'Invalid username/password. Please try again.');
        } 

        $this->response($message,200);

	}


	function register()
	{

		

		if($this->validate_registration())
		{
			$data = $this->input->post();
			$id = $this->global_model->insert('registrants',$data);
		}else
		    echo json_encode(array('error_message'=>$this->input->post()));
		


	}

	function validate_registration()
	{

		$this->load->library('form_validation');

		$config = array(
               array(
                     'field'   => 'firstname', 
                     'label'   => 'Firstname', 
                     'rules'   => 'required|alpha|max_length[50]'
                  ),
               array(
                     'field'   => 'lastname', 
                     'label'   => 'Lastname', 
                     'rules'   => 'required|alpha|max_length[50]'
                  ),
               array(
                     'field'   => 'mobile',
                     'label'   => 'Mobile', 
                     'rules'   => 'required|numeric'
                  ),   
               array(
                     'field'   => 'email', 
                     'label'   => 'Email', 
                     'rules'   => 'required|valid_email'
                  ),   
               array(
                     'field'   => 'password', 
                     'label'   => 'Password', 
                     'rules'   => 'required'
                  ),   
               array(
                     'field'   => 'address', 
                     'label'   => 'Address', 
                     'rules'   => 'required'
                  ),
               array(
                     'field'   => 'bday', 
                     'label'   => '', 
                     'rules'   => 'callback_validate_birthdate'
                  ),
               array(
                     'field'   => 'gender', 
                     'label'   => 'Gender', 
                     'rules'   => 'required'
                  )
            );

		$this->form_validation->set_rules($config);
		return $this->form_validation->run();
	}


	function validate_birthdate()
	{
		$bmonth = $this->post('bmonth');
		$bday = $this->post('bday');
		$byear = $this->post('byear');

		if(!checkdate($bmonth,$bday,$byear)){
			$this->set_message('validate_birthdate','Invalid birthdate.');
			return false;
		}
		return true;

	}


	public function upload_profile()
	{
		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = '../assets/img/registrants';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);	    
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>102,'height'=>102,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $response['new_image'] = $file['file_name'];

	    }else
	    	$response['error'] = $file;
	    
	    echo json_encode($response);

	}


	function resize(){

		$new_size = '100';

		$this->load->helper('file');
		$this->load->helper(array('file','resize'));

		$upload_path = '../assets/img/recipes/resized/';
		$source_path = '../assets/img/recipes/';
		$files = get_dir_file_info($source_path, $top_level_only = TRUE);

		foreach ($files as $v) {
			$img = explode('.', $v['name']);
			$file_name = $img[0];

			if(!file_exists($upload_path.$new_size.'_'.$new_size.'_'.$file_name)){
				$params = array('width'=>$new_size,'height'=>$new_size,'source_image'=>$source_path.'/'.$v['name'],'new_image_path'=>$upload_path,'file_name'=>$v['name']);
				echo resize($params);
			}
 		} 

	}


}