
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Upload extends CI_Controller {

	public function recipe()
	{
		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = '../assets/img/recipes';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);	    
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>100,'height'=>100,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>296,'height'=>296,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $response['new_image'] = $file['file_name'];
 
	    }else
	    	$response['error'] = $file;
	    
	    $this->load->view('json',array('data'=>$response));

	}


	public function article()
	{
		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = '../assets/img/articles';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);	    
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>100,'height'=>100,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>296,'height'=>296,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $response['new_image'] = $file['file_name'];
 
	    }else
	    	$response['error'] = $file;
	    
	    $this->load->view('json',array('data'=>$response));

	}

	public function image_header()
	{
		$this->load->helper('upload');

		$response = array('new_image'=>null,'thumbnail'=>null,'error'=>null);
		$upload_path = '../assets/img/sliders';

		if(!is_dir($upload_path)){
			mkdir($upload_path,0777,true);
		}

		$params = array('upload_path'=>$upload_path,'allowed_types'=>'png|jpg|gif','file_name'=>uniqid(),'max_size'=>'2097152','max_width'=>0,'max_height'=>0,'do_upload'=>'image');
	    $file = upload($params);	    
 
	    if(is_array($file)){

	    	$this->load->helper('resize');
	    	$params = array('width'=>50,'height'=>50,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>200,'height'=>200,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $params = array('width'=>1024,'height'=>342,'source_image'=>$upload_path.'/'.$file['file_name'],'new_image_path'=>$upload_path.'/resized/','file_name'=>$file['file_name']);
 		    resize($params);

 		    $response['new_image'] = $file['file_name'];

	    }else
	    	$response['error'] = $file;
	    
	    $this->load->view('json',array('data'=>$response));

	}

	public function remove_header()
	{
		$image = '../assets/img/sliders/'.$this->input->get('image');
 
		if(file_exists($image)){
			unlink($image);
		}

	}
 

	
}