<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


require APPPATH.'/libraries/REST_Controller.php';


class Registration extends REST_Controller {

	private $base_url = '';

	function __construct(){

		parent:: __construct();
		$this->base_url = str_replace('api/','',base_url());

 	}

	function confirm_registration_get($token){

		$where = array('token'=>$token,'status !='=>'Confirmed');
		$row = $this->global_model->get_row(array('table'=>'registration_token','where'=>$where));

		if($row){

			$this->global_model->update('registration_token',array('status'=>'Confirmed','date_modified'=>array(array('field'=>'date_confirmed','value'=>'NOW()'))),array('registrant_id'=>$row->registrant_id));

			$where = array('registrant_id'=>$row->registrant_id);
			$row = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));


			$confirmed =$this->global_model->update('registrants',array('status'=>'Confirmed', 'date_confirmed'=>date('Y-m-d h:i:s')),array('registrant_id'=>$row->registrant_id));

			// login
			if($confirmed){
				$where = array('registrant_id'=>$row->registrant_id, 'status'=>'Confirmed');
				$login = $this->global_model->get_row(array('table'=>'registrants','where'=>$where));

		        	if($login) {
		        		$this->session->set_userdata(array('registrant_id'=>$login->registrant_id,'registrant_name'=>$login->firstname.'-'.$login->lastname,'registrant_logged_in'=>true));           
			            	$message = array('error'=>0,'message'=>'Successfully logged in.','session'=>$this->session->all_userdata(),'first_visit'=>$login->first_visit); 

			            	if( $login->first_visit ) {
			            		$this->db->where('registrant_id', $login->registrant_id)->update('tbl_registrants', array('first_visit' => 0));
			            	}
		
			            	return redirect('http://www.bountyfreshmoms.com/pledging');
		        	}
		    }
			//

			$this->response(array('status'=>'success','message'=>'Your registration has been successfully confirmed.'),200);
		}else{
			return redirect('http://www.bountyfreshmoms.com/login');
			// $this->response(array('status'=>'failed','message'=>'Confirmation token is not found/active.','token'=>$this->post('token')),200);
		}


	}
}
