<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function resize($params)
{
	$CI =& get_instance();
	$CI->load->library('image_lib');
	
	# Settings
	$new_filename = isset($params['new_filename']) ? $params['new_filename'] : $params['width'].'_'.$params['height'].'_'.$params['file_name'];
	
	$config['image_library'] = 'gd2';
	$config['source_image'] =  $params['source_image'];
	$config['new_image'] = $params['new_image_path'].$new_filename;
	$config['width'] = $params['width'];
	$config['height'] = $params['height'];
	
	# Load library
	$CI->image_lib->initialize($config);
	$CI->image_lib->resize();
	return $CI->image_lib->display_errors();
	
	
}