<?php 
 class Login_check {

	public function check()
	{
		$CI = & get_instance();
 		$controller = $CI->uri->segment(1);
		$method = $CI->uri->segment(2);
		$session = $CI->session->userdata('admin_logged_in');
		$except_controllers = array('bounty');
 		
		if(!$session && $controller != 'login' && !in_array($controller,$except_controllers) ){
			
			exit(json_encode(array('login_status'=>'failed')));
			
		}
 		
	}
	
}